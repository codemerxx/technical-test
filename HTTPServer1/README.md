# Extensible modular multi-threaded protocol-agnostic web server library
The web server library enables writing highly configurable multi-threaded web server implementations, based on TCP/IP. It is protocol-agnostic and can support any TCP/IP protocol such as HTTP, FTP, SMTP, SSH, etc. HTTP protocol module is provided with support for HTTP/1.1 persistent connections.

## Architecture overview
The main entry point to the library is the [WebServer](/web-server/src/main/java/com/codemerx/webserver/WebServer.java) class, instantiated with [WebServerConfiguration](/web-server/src/main/java/com/codemerx/webserver/configuration/WebServerConfiguration.java).

The `WebServerConfiguration` contains vital settings for starting and running the server such as port, thread-pool size and the actual request [Handler](/web-server/src/main/java/com/codemerx/webserver/handlers/Handler.java). The [WebServerConfigurationBuilder](/web-server/src/main/java/com/codemerx/webserver/configuration/WebServerConfigurationBuilder.java) provides fluent builder API for creating `WebServerConfiguration` instances.

Starting the `WebServer` is done by calling its `tryStart()` method. It uses the configuration to open a TCP socket waiting for incoming connections on the configured port and initializes a fixed-size thread pool using the thread-pool size from the configuration. It also starts a worker thread waiting for incoming connections. When a client connects, the worker thread passes the connection socket to thread-pool thread for processing and continues listening for new incoming connections. The thread-pool thread uses the request handler from the configuration to process the request.

[Handler](/web-server/src/main/java/com/codemerx/webserver/handlers/Handler.java) is an abstraction of protocol processor, which can be implement to add support for any protocol, such as HTTP, FTP, WebSockets, etc. Implementation for HTTP is provided by the [HttpHandler](/web-server/src/main/java/com/codemerx/webserver/http/HttpHandler.java).

The `HttpHandler` is modular middleware-based processor for HTTP requests. [Middleware](/web-server/src/main/java/com/codemerx/webserver/http/middlewares/Middleware.java) is a single responsibility pipeline unit, used to process a request and build a response for it. The middleware pipeline approach is inspired by the [Chain of responsibility](https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern) pattern.

[HttpHandlerBuilder](/web-server/src/main/java/com/codemerx/webserver/http/HttpHandlerBuilder.java) is a fluent builder API for configuration and creation of `HttpHandler` instances. It allows providing an unlimited amount of `Middleware` instances. The `Middleware` interface is designed as functional interface in order to enable passing in lambdas, where simple middleware is needed. [LoggingMiddleware](/web-server/src/main/java/com/codemerx/webserver/http/middlewares/LoggingMiddleware.java) is simple middleware adding logging of every request and response. As such it is usually placed at the beginning of the middleware pipeline.

# HTTP File server
File server application, built using the Web server library. It uses the modular middleware-based architecture of `HttpClient` and provides custom `Middleware` - [FileServingMiddleware](/file-server/src/main/java/com/codemerx/fileserver/middlewares/FileServingMiddleware.java), handling the HTTP request by returning files from the file system.

It is configurable using a config file - `config.json`. The application expects the config file to be in the same directory as the resulting .jar file. If used without config file default values are used. The config file uses the following schema:
```
{
    "serveDirectory": string,
    "port": integer,
    "connectionHandlers": integer,
    "requestHandlers": integer,
    "connectionTimeout": integer
}
```
- `serverDirectory` - absolute or relative path to the directory that should be served by the file server (defaults to the directory of the .jar file). Note: backslashes must be escaped with another backslash, i.e. `\\`
- `port` - the port at which the file server should wait for incoming connections (defaults to automatically selected free port)
- `connectionHandlers` - the number of thread-pool threads, that should be used for handling incoming connections
- `requestHandlers` - the number of thread-pool threads, that should be used for processing requests on a given connection
- `connectionTimeout` - idle connection timeout (in seconds), i.e. the maximum time between two consecutive requests. `0` - the server will wait for requests indefinitely

# Instructions

## Prerequisites
- Java 12

## Starting demo
A pre-compiled version of the File server application is setup in the [dist](/dist) folder, alongside a `config.json` and a sample directory to serve. The demo can be started with the following command:

`java -jar dist\file-server-2.0.0-precompiled.jar` (Windows)

`java -jar dist/file-server-2.0.0-precompiled.jar` (Unix)

## Build and run tests

`gradlew fatJar` (Windows)

`sh gradlew fatJar` (Unix)

The resulting jar is outputted to the [dist](/dist) folder as `file-server-2.0.0.jar`.

## Run

`java -jar dist\file-server-2.0.0.jar` (Windows)

`java -jar dist/file-server-2.0.0.jar` (Unix)