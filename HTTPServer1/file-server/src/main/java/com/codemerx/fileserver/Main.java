package com.codemerx.fileserver;

import com.codemerx.fileserver.configuration.Configuration;
import com.codemerx.fileserver.middlewares.FileServingMiddleware;
import com.codemerx.webserver.*;
import com.codemerx.webserver.configuration.*;
import com.codemerx.webserver.http.handler.HttpHandlerBuilder;
import com.codemerx.webserver.http.middlewares.*;

import java.nio.file.*;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = Configuration.read();
        FileServingMiddleware fileServingMiddleware = createFileServingMiddleware(configuration.serveDirectory);
        Handler handler = HttpHandlerBuilder.initialize()
                                            .with(Configuration.LOGGER)
                                            .withRequestHandlers(configuration.requestHandlers)
                                            .withConnectionTimeout(configuration.connectionTimeout)
                                            .use(new LoggingMiddleware(Configuration.LOGGER))
                                            .use(fileServingMiddleware)
                                            .build();
        WebServerConfiguration webServerConfiguration = WebServerConfigurationBuilder.initialize(handler)
                                                                                     .withLogger(Configuration.LOGGER)
                                                                                     .onPort(configuration.port)
                                                                                     .withConnectionHandlers(configuration.connectionHandlers)
                                                                                     .build();
        WebServer server = new WebServer(webServerConfiguration);
        if (server.tryStart()) {
            Configuration.LOGGER.info("Serving files from " + configuration.serveDirectory);
        } else {
            System.exit(2);
        }
    }

    private static FileServingMiddleware createFileServingMiddleware(String rootDirectory) {
        try {
            return new FileServingMiddleware(rootDirectory, Configuration.LOGGER);
        } catch (NoSuchFileException | NotDirectoryException e) {
            Configuration.LOGGER.error("Invalid path for file serving - " + rootDirectory);
            System.exit(1);
            return null;
        }
    }
}
