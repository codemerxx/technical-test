package com.codemerx.fileserver.configuration;

import com.codemerx.webserver.logging.*;
import com.google.gson.*;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.*;

public class Configuration {
    public static final Logger LOGGER = new ConsoleLogger(new DefaultFormatter());
    private static Path parentDirectory;
    private static final Gson gson = new Gson();
    private static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    private static final String DEFAULT_SERVE_DIRECTORY = "./";
    private static final int DEFAULT_PORT = 0;
    private static final int DEFAULT_CONNECTION_HANDLERS = AVAILABLE_PROCESSORS * 100;
    private static final int DEFAULT_REQUEST_HANDLERS = AVAILABLE_PROCESSORS;
    private static final int DEFAULT_CONNECTION_TIMEOUT = 10;

    public String serveDirectory = DEFAULT_SERVE_DIRECTORY;
    public int port = DEFAULT_PORT;
    public int connectionHandlers = DEFAULT_CONNECTION_HANDLERS;
    public int requestHandlers = DEFAULT_REQUEST_HANDLERS;
    public int connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

    static {
        try {
            parentDirectory = new File(Configuration.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile().toPath();
        } catch (URISyntaxException ignored) { }
    }

    public static Configuration read() {
        try {
            Path configFilePath = Paths.get(parentDirectory.toString(),"config.json");
            File configFile = configFilePath.toFile();
            if (configFile.exists() && !configFile.isDirectory()) {
                String configFileContents;
                try {
                    configFileContents = Files.readString(configFilePath);
                    if (configFileContents.length() == 0) {
                        LOGGER.warning("Empty config.json, continuing with default settings...");
                        return getDefaultConfiguration();
                    }
                } catch (IOException e) {
                    LOGGER.warning("Failed to read config.json, continuing with default settings...");
                    return getDefaultConfiguration();
                }
                Configuration result;
                try {
                    result = gson.fromJson(configFileContents, Configuration.class);
                } catch (JsonSyntaxException e) {
                    LOGGER.warning("Failed to parse config.json, continuing with default settings...");
                    return getDefaultConfiguration();
                }
                result.validate();
                result.normalize();
                return result;
            }

            LOGGER.warning("config.json not found, continuing with default settings...");
            return getDefaultConfiguration();
        } catch (Exception e) {
            LOGGER.warning("Processing config.json failed, continuing with default settings...");
            return getDefaultConfiguration();
        }
    }

    private static Configuration getDefaultConfiguration() {
        Configuration result = new Configuration();
        result.normalize();
        return result;
    }

    private void validate() {
        if (this.port < 0 || this.port > 65535) {
            LOGGER.warning("Invalid value for port. It should be in the range [0, 65535]");
            this.port = DEFAULT_PORT;
        }

        if (this.connectionHandlers <= 0) {
            LOGGER.warning("Invalid value for connectionHandlers. It should be in the range [1,)");
            this.connectionHandlers = DEFAULT_CONNECTION_HANDLERS;
        }

        if (this.requestHandlers <= 0) {
            LOGGER.warning("Invalid value for requestHandlers. It should be in the range [1,)");
            this.requestHandlers = DEFAULT_REQUEST_HANDLERS;
        }

        if (this.connectionTimeout < 0) {
            LOGGER.warning("Invalid value for connectionTimeout. It should be in the range [0,)");
            this.connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
        }
    }

    private void normalize() {
        Path rootPath = Paths.get(this.serveDirectory);
        if (!rootPath.isAbsolute()) {
            rootPath = parentDirectory.resolve(rootPath.normalize()).normalize();
        }
        this.serveDirectory = rootPath.toString();
    }
}
