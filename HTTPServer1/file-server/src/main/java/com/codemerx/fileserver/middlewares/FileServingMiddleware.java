package com.codemerx.fileserver.middlewares;

import com.codemerx.webserver.http.*;
import com.codemerx.webserver.http.handler.HttpContext;
import com.codemerx.webserver.http.middlewares.Middleware;
import com.codemerx.webserver.http.types.*;
import com.codemerx.webserver.logging.Logger;

import java.io.*;
import java.nio.file.*;
import java.util.Objects;

public class FileServingMiddleware implements Middleware {
    private final Path root;
    private final Logger logger;

    public FileServingMiddleware(String rootPath, Logger logger) throws NoSuchFileException, NotDirectoryException {
        Path path = Paths.get(rootPath).normalize();
        File file = path.toFile();
        if (!file.exists()) {
            throw new NoSuchFileException(path.toString());
        } else if (!file.isDirectory()) {
            throw new NotDirectoryException(path.toString());
        }

        this.root = path.toAbsolutePath();
        this.logger = logger;
    }

    @Override
    public void invoke(HttpContext context, HttpProcessor next) throws Exception {
        HttpRequestLine requestLine = context.getRequest().getRequestLine();
        HttpMethod method = requestLine.getMethod();
        if (!(Objects.equals(method, HttpMethod.GET) || Objects.equals(method, HttpMethod.HEAD))) {
            HttpResponse response = HttpResponseFactory.INSTANCE.create(HttpStatusCode.METHOD_NOT_ALLOWED);
            response.addHeader(HttpHeaderName.ALLOW, HttpMethod.GET + ", " + HttpMethod.HEAD);
            context.setResponse(response);
            return;
        }

        String resource = uriToRelativePath(requestLine.getUri().getPath());
        Path path;
        try {
            path = this.root.resolve(resource).normalize();
        } catch (InvalidPathException e) {
            context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.BAD_REQUEST));
            return;
        }

        if (!path.startsWith(this.root)) {
            context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.FORBIDDEN));
            return;
        }

        File file = path.toFile();
        if (!file.exists()) {
            context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.NOT_FOUND));
            return;
        }

        if (file.isDirectory()) {
            Path indexHtmlPath = path.resolve("index.html");
            File indexHtml = indexHtmlPath.toFile();
            if (indexHtml.exists() && !indexHtml.isDirectory()) {
                String indexHtmlUri = this.root.relativize(indexHtmlPath).toString();
                HttpResponse response = HttpResponseFactory.INSTANCE.create(HttpStatusCode.TEMPORARY_REDIRECT);
                response.addHeader(HttpHeaderName.LOCATION, relativePathToUri(indexHtmlUri));
                context.setResponse(response);
                return;
            } else {
                context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.FORBIDDEN));
                return;
            }
        }

        HttpResponse response;
        if (method.equals(HttpMethod.GET)) {
            FileInputStream fileStream;
            try {
                fileStream = new FileInputStream(file);
            } catch (FileNotFoundException | SecurityException e) {
                this.logger.error(e);
                context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.INTERNAL_SERVER_ERROR));
                return;
            }

            response = HttpResponseFactory.INSTANCE.create(HttpStatusCode.OK, fileStream);
        } else { // HEAD
            response = HttpResponseFactory.INSTANCE.create(HttpStatusCode.OK);
        }

        try {
            long fileLength = file.length();
            response.addHeader(HttpHeaderName.CONTENT_LENGTH, Long.toString(fileLength));
        } catch (SecurityException e) {
            this.logger.error(e);
            context.setResponse(HttpResponseFactory.INSTANCE.create(HttpStatusCode.INTERNAL_SERVER_ERROR));
            return;
        }

        try {
            String contentType = Files.probeContentType(path);
            if (contentType != null) {
                response.addHeader(HttpHeaderName.CONTENT_TYPE, contentType);
            }
        } catch (IOException | SecurityException e) {
            this.logger.error(e);
        }

        context.setResponse(response);
    }

    private static String uriToRelativePath(String uri) {
        // Drop the leading forward slash
        return uri.substring(1);
    }

    private static String relativePathToUri(String relativePath) {
        return "/" + relativePath.replace('\\', '/');
    }
}
