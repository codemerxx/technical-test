package com.codemerx.fileserver.tests;

import org.apache.http.*;
import org.apache.http.message.BasicHeader;

public abstract class EphemeralConnectionTests extends WebServerTests {
    @Override
    protected Header[] getDefaultHeaders() {
        return new Header[] { new BasicHeader(HttpHeaders.CONNECTION, "Close") };
    }
}
