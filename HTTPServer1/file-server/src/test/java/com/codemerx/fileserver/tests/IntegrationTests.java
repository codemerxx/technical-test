package com.codemerx.fileserver.tests;

import org.apache.http.*;
import org.apache.http.client.methods.*;
import org.junit.*;

import java.io.*;

public class IntegrationTests extends EphemeralConnectionTests {
    @Test
    public void getFile() throws IOException {
        HttpResponse response = this.get("/file.txt");

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        this.assertFileHttpResponse(response);
        Assert.assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", result.toString());
    }

    @Test
    public void headFile() {
        HttpResponse response = this.execute(new HttpHead(baseUrl + "/file.txt"), null);
        this.assertFileHttpResponse(response);
    }

    private void assertFileHttpResponse(HttpResponse response) {
        Assert.assertEquals(200, response.getStatusLine().getStatusCode());
        this.assertHeaderValue(response, HttpHeaders.CONTENT_LENGTH, Integer.toString(56));
        this.assertHeaderValue(response, HttpHeaders.CONNECTION, "Close");
        Assert.assertEquals(1, response.getHeaders(HttpHeaders.SERVER).length);
        Assert.assertEquals(1, response.getHeaders(HttpHeaders.DATE).length);
    }

    @Test
    public void getNonExistingFile() {
        HttpResponse response = this.get("/missing.txt");
        Assert.assertEquals(404, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getDirectory() {
        HttpResponse response = this.get("/directory");
        Assert.assertEquals(403, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getFileOutsideRootDirectory() {
        HttpResponse response = this.get("/../directory");
        Assert.assertEquals(403, response.getStatusLine().getStatusCode());
    }

    @Test
    public void unsupportedHttpMethod() {
        HttpResponse response = this.execute(new HttpPut(baseUrl + "/file.txt"), null);
        Assert.assertEquals(405, response.getStatusLine().getStatusCode());
        this.assertHeaderValue(response, HttpHeaders.ALLOW, "GET, HEAD");
    }

    @Test
    public void getDirectoryWithIndexHtml() {
        HttpResponse response = this.get("/directoryWithIndexHtml");
        Assert.assertEquals(307, response.getStatusLine().getStatusCode());
        this.assertHeaderValue(response, HttpHeaders.LOCATION, "/directoryWithIndexHtml/index.html");
    }
}
