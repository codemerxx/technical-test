package com.codemerx.fileserver.tests;

import org.apache.http.HttpResponse;
import org.junit.*;

import java.io.*;
import java.util.concurrent.*;

public class LoadTests extends PersistentConnectionTests {
    @Test
    public void get10000() throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < 10000; i++) {
            threadPool.submit(() -> {
                HttpResponse response = this.get("/file.txt");

                StringBuffer result = new StringBuffer();
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                } catch (IOException e) {
                    Assert.fail();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) { }
                    }
                }

                Assert.assertEquals(200, response.getStatusLine().getStatusCode());
                Assert.assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", result.toString());
            });
        }

        threadPool.shutdown();
        threadPool.awaitTermination(2, TimeUnit.MINUTES);
    }
}
