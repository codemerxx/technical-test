package com.codemerx.fileserver.tests;

import org.apache.http.*;
import org.apache.http.message.BasicHeader;
import org.junit.*;

import java.io.*;

public class PersistentConnectionIntegrationTests extends PersistentConnectionTests {
    @Test
    public void getFiles() throws IOException {
        this.getFile("1", "Keep-Alive");
        this.getFile("2", "Keep-Alive");
        this.getFile("3", new Header[] { new BasicHeader(HttpHeaders.CONNECTION, "Close") }, "Close");
    }

    private void getFile(String file, String expectedConnectionHeaderValue) throws IOException {
        this.getFile(file, null, expectedConnectionHeaderValue);
    }

    private void getFile(String file, Header[] headers, String expectedConnectionHeaderValue) throws IOException {
        HttpResponse response = this.get("/persistent/" + file + ".txt", headers);

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        Assert.assertEquals(200, response.getStatusLine().getStatusCode());
        this.assertHeaderValue(response, HttpHeaders.CONTENT_LENGTH, Integer.toString(1));
        this.assertHeaderValue(response, HttpHeaders.CONNECTION, expectedConnectionHeaderValue);
        Assert.assertEquals(file, result.toString());
    }
}
