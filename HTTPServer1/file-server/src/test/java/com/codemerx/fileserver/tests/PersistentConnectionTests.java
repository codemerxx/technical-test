package com.codemerx.fileserver.tests;

import org.apache.http.*;
import org.apache.http.message.*;

public class PersistentConnectionTests extends WebServerTests {
    @Override
    protected Header[] getDefaultHeaders() {
        return new Header[] { new BasicHeader(HttpHeaders.CONNECTION, "Keep-Alive") };
    }
}
