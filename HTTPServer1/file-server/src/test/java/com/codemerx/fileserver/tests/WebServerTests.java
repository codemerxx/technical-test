package com.codemerx.fileserver.tests;

import com.codemerx.webserver.*;
import com.codemerx.webserver.configuration.*;
import com.codemerx.webserver.http.handler.HttpHandlerBuilder;
import com.codemerx.webserver.http.middlewares.LoggingMiddleware;
import com.codemerx.fileserver.middlewares.FileServingMiddleware;
import com.codemerx.webserver.logging.*;
import org.apache.http.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.*;

import java.io.IOException;
import java.nio.file.*;

public abstract class WebServerTests {
    protected static String baseUrl;
    private static WebServer server;
    private static CloseableHttpClient client;

    protected abstract Header[] getDefaultHeaders();

    @BeforeClass
    public static void setup() throws NoSuchFileException, NotDirectoryException {
        ConsoleLogger logger = new ConsoleLogger(new DefaultFormatter());
        Handler handler = HttpHandlerBuilder.initialize()
                                            .with(logger)
                                            .use(new LoggingMiddleware(logger))
                                            .use(new FileServingMiddleware("src/test/resources/fixtures", logger))
                                            .build();
        WebServerConfiguration configuration = WebServerConfigurationBuilder.initialize(handler)
                                                                            .withLogger(logger)
                                                                            .build();
        server = new WebServer(configuration);
        if (!server.tryStart()) {
            Assert.fail();
        }

        baseUrl = "http://localhost:" + configuration.getPort();

        client = HttpClientBuilder.create()
                                  .setConnectionManager(new PoolingHttpClientConnectionManager())
                                  .disableRedirectHandling()
                                  .build();
    }

    @AfterClass
    public static void teardown() {
        try {
            client.close();
        } catch (IOException e) { }
    }

    protected HttpResponse get(String url) {
        return this.get(url, null);
    }

    protected HttpResponse get(String url, Header[] headers) {
        return this.execute(new HttpGet(baseUrl + url), headers);
    }

    protected HttpResponse execute(HttpUriRequest request, Header[] headers) {
        Header[] requestHeaders = headers != null ? headers : this.getDefaultHeaders();
        for (Header header: requestHeaders) {
            request.setHeader(header);
        }
        try {
            return client.execute(request);
        } catch (IOException e) {
            return null;
        }
    }

    protected void assertHeaderValue(HttpResponse response, String name, String expectedValue) {
        Header[] headers = response.getHeaders(name);
        Assert.assertEquals(1, headers.length);
        Assert.assertEquals(expectedValue, headers[0].getValue());
    }
}
