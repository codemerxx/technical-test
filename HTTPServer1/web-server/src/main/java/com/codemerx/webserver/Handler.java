package com.codemerx.webserver;

import java.net.Socket;

public interface Handler {
    void handle(Socket connection) throws Exception;
}
