package com.codemerx.webserver;

import com.codemerx.webserver.configuration.WebServerConfiguration;
import com.codemerx.webserver.logging.Logger;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.*;

public class WebServer {
    private WebServerConfiguration configuration;

    private ServerSocket connectionAcceptor;
    private ExecutorService threadPool;

    public WebServer(WebServerConfiguration configuration) {
        this.configuration = configuration;
    }

    public boolean tryStart() {
        Logger logger = this.configuration.getLogger();
        try {
            this.connectionAcceptor = new ServerSocket(this.configuration.getPort());
        } catch (IOException | SecurityException | IllegalArgumentException e) {
            logger.info("Server failed to start");
            logger.error(e);
            return false;
        }

        if (this.configuration.getPort() == 0) {
            this.configuration.setPort(this.connectionAcceptor.getLocalPort());
        }
        logger.info("Server listening at port " + this.connectionAcceptor.getLocalPort());

        int connectionHandlers = this.configuration.getConnectionHandlers();
        logger.info("Initializing thread pool with " + connectionHandlers + " connection handler" + (connectionHandlers > 1 ? "s" : ""));
        this.threadPool = Executors.newFixedThreadPool(connectionHandlers);
        new Thread(() -> {
            Handler handler = this.configuration.getHandler();
            while (true) {
                Socket connection;
                try {
                    connection = connectionAcceptor.accept();
                } catch (IOException | SecurityException e) {
                    logger.error(e);
                    continue;
                }
                threadPool.submit(() -> {
                    logger.info("Client connected");
                    try {
                        handler.handle(connection);
                    } catch (Exception e) {
                        logger.error(e);
                    } finally {
                        if (!connection.isClosed()) {
                            try {
                                connection.close();
                            } catch (IOException e) {
                                logger.error(e);
                            }
                        }
                    }
                    logger.info("Connection closed");
                });
            }
        }).start();

        return true;
    }
}
