package com.codemerx.webserver.configuration;

import com.codemerx.webserver.Handler;
import com.codemerx.webserver.logging.*;

public class WebServerConfiguration {
    private Handler handler;
    private int port;
    private int connectionHandlers;
    private Logger logger;

    WebServerConfiguration(Handler handler, int port, int connectionHandlers, Logger logger) {
        if (handler == null) {
            throw new IllegalArgumentException("handler cannot be null");
        }
        validatePort(port);
        if (connectionHandlers < 1) {
            throw new IllegalArgumentException("connectionHandlers should be in the range [1,)");
        }

        this.handler = handler;
        this.port = port;
        this.connectionHandlers = connectionHandlers;
        this.logger = logger;
    }

    public Handler getHandler() {
        return this.handler;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        validatePort(port);
        this.port = port;
    }

    public int getConnectionHandlers() {
        return this.connectionHandlers;
    }

    public Logger getLogger() {
        return this.logger;
    }

    private static void validatePort(int port) {
        if (port < 0 || port > 65535) {
            throw new IllegalArgumentException("port should be in the range [0, 65535]");
        }
    }
}
