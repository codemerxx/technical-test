package com.codemerx.webserver.configuration;

import com.codemerx.webserver.Handler;
import com.codemerx.webserver.logging.*;

public class WebServerConfigurationBuilder {
    private Handler handler;
    private int port = 0;
    private int connectionHandlers = Runtime.getRuntime().availableProcessors() * 100;
    private Logger logger = new NoopLogger();

    private WebServerConfigurationBuilder(Handler handler) {
        this.handler = handler;
    }

    public static WebServerConfigurationBuilder initialize(Handler handler) {
        return new WebServerConfigurationBuilder(handler);
    }

    public WebServerConfigurationBuilder onPort(int port) {
        this.port = port;
        return this;
    }

    public WebServerConfigurationBuilder withConnectionHandlers(int connectionHandlers) {
        this.connectionHandlers = connectionHandlers;
        return this;
    }

    public WebServerConfigurationBuilder withLogger(Logger logger) {
        this.logger = logger;
        return this;
    }

    public WebServerConfiguration build() {
        return new WebServerConfiguration(this.handler, this.port, this.connectionHandlers, this.logger);
    }
}
