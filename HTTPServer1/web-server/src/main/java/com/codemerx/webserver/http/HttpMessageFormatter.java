package com.codemerx.webserver.http;

import com.codemerx.webserver.http.types.*;

public class HttpMessageFormatter {
    public static final HttpMessageFormatter INSTANCE = new HttpMessageFormatter();

    private HttpMessageFormatter() { }

    public String format(HttpRequest request) {
        HttpRequestLine requestLine = request.getRequestLine();
        return requestLine.getMethod() + " " + requestLine.getUri().getPath();
    }

    public String format(HttpResponse response) {
        HttpStatusLine statusLine = response.getStatusLine();
        return statusLine.getStatusCode().getCode() + " " + statusLine.getStatusCode().getReasonPhrase();
    }
}
