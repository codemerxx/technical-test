package com.codemerx.webserver.http;

import com.codemerx.webserver.http.handler.HttpContext;

public interface HttpProcessor {
    void process(HttpContext context) throws Exception;
}
