package com.codemerx.webserver.http;

import com.codemerx.webserver.http.types.*;

import java.io.InputStream;

public class HttpResponseFactory {
    public static final HttpResponseFactory INSTANCE = new HttpResponseFactory();

    private HttpResponseFactory() { }

    public HttpResponse create(HttpStatusCode statusCode) {
        return this.create(statusCode, null);
    }

    public HttpResponse create(HttpStatusCode statusCode, InputStream body) {
        HttpStatusLine statusLine = new HttpStatusLine(HttpVersion.HTTPv1_1, statusCode);
        HttpResponse response = new HttpResponse(statusLine);
        if (body != null) {
            response.setBody(body);
        }

        return response;
    }
}
