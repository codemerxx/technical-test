package com.codemerx.webserver.http.handler;

import com.codemerx.webserver.http.types.*;

class Helpers {
    static final Helpers INSTANCE = new Helpers();

    private Helpers() { }

    boolean isPersistentConnectionRequest(HttpRequest request) {
        if (request.hasHeader(HttpHeaderName.CONNECTION)) {
            String connectionHeaderValue = request.getHeader(HttpHeaderName.CONNECTION);
            return HttpHeaderValues.KEEP_ALIVE.equalsIgnoreCase(connectionHeaderValue);
        } else {
            return request.getRequestLine().getHttpVersion() == HttpVersion.HTTPv1_1;
        }
    }

    boolean shouldHaveContentLengthHeader(HttpResponse response) {
        HttpStatusLine statusLine = response.getStatusLine();
        return statusLine.getStatusCode() != HttpStatusCode.CONTINUE &&
               statusLine.getStatusCode() != HttpStatusCode.SWITCHING_PROTOCOLS &&
               statusLine.getStatusCode() != HttpStatusCode.NO_CONTENT &&
               statusLine.getStatusCode() != HttpStatusCode.NOT_MODIFIED;
    }
}
