package com.codemerx.webserver.http.handler;

import com.codemerx.webserver.http.types.*;

public class HttpContext {
    private HttpRequest request;
    private HttpResponse response;
    private boolean isCompleted = false;

    HttpContext(HttpRequest request) {
        this.request = request;
    }

    HttpContext(HttpResponse response) {
        this.response = response;
    }

    public HttpRequest getRequest() {
        return this.request;
    }

    public HttpResponse getResponse() {
        return this.response;
    }

    public void setResponse(HttpResponse response) {
        if (response == null) {
            throw new IllegalArgumentException("response cannot be null");
        }

        this.response = response;
    }

    synchronized void complete() {
        this.isCompleted = true;
        this.notify();
    }

    synchronized void waitForCompletion() {
        while (!this.isCompleted) {
            try {
                this.wait();
            } catch (InterruptedException ignored) { }
        }
    }
}
