package com.codemerx.webserver.http.handler;

import java.time.*;
import java.time.format.DateTimeFormatter;

class HttpDateTimeProvider {
    static final HttpDateTimeProvider INSTANCE = new HttpDateTimeProvider();

    private final DateTimeFormatter formatter;

    private HttpDateTimeProvider() {
        this.formatter = DateTimeFormatter.RFC_1123_DATE_TIME;
    }

    String getCurrentTime() {
        return this.formatter.format(OffsetDateTime.now(ZoneOffset.UTC));
    }
}
