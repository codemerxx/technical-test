package com.codemerx.webserver.http.handler;

import com.codemerx.webserver.Handler;
import com.codemerx.webserver.http.*;
import com.codemerx.webserver.http.reader.*;
import com.codemerx.webserver.http.types.*;
import com.codemerx.webserver.http.writer.*;
import com.codemerx.webserver.logging.Logger;

import java.io.*;
import java.net.*;
import java.util.concurrent.*;

class HttpHandler implements Handler {
    private static final String SERVER_NAME = "CodeMerx WebServer";
    private static final int DEFAULT_REQUEST_PROCESS_TIMEOUT = 30;

    private final HttpProcessor processor;
    private final int requestHandlers;
    private final int connectionTimeout;
    private final Logger logger;
    private final HttpRequestReaderFactory requestReaderFactory;
    private final HttpResponseWriterFactory responseWriterFactory;

    HttpHandler(HttpProcessor processor, int requestHandlers, int connectionTimeout, Logger logger, HttpRequestReaderFactory requestReaderFactory, HttpResponseWriterFactory responseWriterFactory) {
        this.processor = processor;
        this.requestHandlers = requestHandlers;
        this.connectionTimeout = connectionTimeout;
        this.logger = logger;
        this.requestReaderFactory = requestReaderFactory;
        this.responseWriterFactory = responseWriterFactory;
    }

    @Override
    public void handle(Socket connection) throws InterruptedException, IOException {
        try {
            InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = connection.getOutputStream();

            connection.setSoTimeout(this.connectionTimeout * 1000);

            HttpRequestReader reader = this.requestReaderFactory.create(inputStream);
            HttpResponseWriter writer = this.responseWriterFactory.create(outputStream);

            ReadRequestResult readResult;
            try {
                readResult = this.tryReadRequest(reader);
            } catch (SocketTimeoutException ignored) {
                this.logger.info("Connection timeout");
                return;
            }

            HttpRequest request = readResult.request;
            if (request == null) {
                HttpResponse errorResponse = readResult.errorResponse;
                if (errorResponse != null) {
                    this.logger.info(HttpMessageFormatter.INSTANCE.format(errorResponse));
                    HttpContext errorContext = new HttpContext(errorResponse);
                    this.sendResponse(writer, errorContext);
                }

                return;
            }

            if (Helpers.INSTANCE.isPersistentConnectionRequest(request)) {
                this.handlePersistentConnectionRequest(reader, writer, request);
            } else {
                HttpContext context = new HttpContext(request);
                HttpRequestHandler handler = new HttpRequestHandler(context);
                handler.run();
                this.sendResponse(writer, context);
            }
        } finally {
            this.logger.info("Closing connection...");
            this.close(connection);
        }
    }

    private void handlePersistentConnectionRequest(HttpRequestReader reader, HttpResponseWriter writer, HttpRequest initialRequest) throws InterruptedException, IOException {
        this.logger.info("Persistent connection request received");
        PersistentConnectionRequestQueue queue = new PersistentConnectionRequestQueue();
        PersistentConnectionResponseSender responseSender = new PersistentConnectionResponseSender(writer, queue);
        Thread responseSenderThread = new Thread(responseSender);
        responseSenderThread.start();
        this.logger.info("Initializing thread pool with " + this.requestHandlers + " request handler" + (this.requestHandlers > 1 ? "s" : ""));
        ExecutorService requestHandlers = Executors.newFixedThreadPool(this.requestHandlers);
        try {
            HttpContext initialRequestContext = new HttpContext(initialRequest);
            queue.add(initialRequestContext);
            requestHandlers.submit(new HttpRequestHandler(initialRequestContext));

            while (true) {
                ReadRequestResult readResult;
                try {
                    readResult = this.tryReadRequest(reader);
                } catch (SocketTimeoutException ignored) {
                    this.logger.info("Persistent connection timeout");
                    break;
                }

                if (responseSender.hasException()) {
                    this.logger.error("Error occurred while sending responses");
                    this.logger.error(responseSender.getException());
                    break;
                }

                HttpRequest request = readResult.request;
                if (request == null) {
                    HttpResponse errorResponse = readResult.errorResponse;
                    if (errorResponse != null) {
                        this.logger.info(HttpMessageFormatter.INSTANCE.format(errorResponse));
                        HttpContext errorContext = new HttpContext(errorResponse);
                        errorContext.complete();
                        queue.add(errorContext);
                    }

                    break;
                }

                HttpContext context = new HttpContext(request);
                queue.add(context);
                requestHandlers.submit(new HttpRequestHandler(context));
                if (!Helpers.INSTANCE.isPersistentConnectionRequest(request)) {
                    break;
                }
            }
        } finally {
            this.logger.info("Closing persistent connection...");
            requestHandlers.shutdown();
            if (responseSenderThread.isAlive()) {
                requestHandlers.awaitTermination(DEFAULT_REQUEST_PROCESS_TIMEOUT, TimeUnit.SECONDS);
                queue.complete();
                responseSenderThread.join();
            }
        }
    }

    private ReadRequestResult tryReadRequest(HttpRequestReader reader) throws IOException {
        HttpRequest request = null;
        HttpResponse errorResponse = null;
        try {
            request = reader.read();
        } catch (HttpMethodNotImplementedException e) {
            this.logger.info(e.getMessage());
            errorResponse = HttpResponseFactory.INSTANCE.create(HttpStatusCode.NOT_IMPLEMENTED);
        } catch (HttpRequestParseException | URISyntaxException e) {
            this.logger.info(e.getMessage());
            errorResponse = HttpResponseFactory.INSTANCE.create(HttpStatusCode.BAD_REQUEST);
        } catch (HttpVersionNotSupportedException e) {
            this.logger.info(e.getMessage());
            errorResponse = HttpResponseFactory.INSTANCE.create(HttpStatusCode.HTTP_VERSION_NOT_SUPPORTED);
        }

        return new ReadRequestResult(request, errorResponse);
    }

    private void sendResponse(HttpResponseWriter writer, HttpContext context) throws IOException {
        HttpRequest request = context.getRequest();
        HttpResponse response = context.getResponse();

        response.addHeader(HttpHeaderName.SERVER, SERVER_NAME);
        response.addHeader(HttpHeaderName.DATE, HttpDateTimeProvider.INSTANCE.getCurrentTime());
        if (!response.hasHeader(HttpHeaderName.CONTENT_LENGTH) && Helpers.INSTANCE.shouldHaveContentLengthHeader(response)) {
            response.addHeader(HttpHeaderName.CONTENT_LENGTH, "0");
        }
        if (request != null && Helpers.INSTANCE.isPersistentConnectionRequest(request)) {
            response.addHeader(HttpHeaderName.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        } else {
            response.addHeader(HttpHeaderName.CONNECTION, HttpHeaderValues.CLOSE);
        }

        writer.write(response);
    }

    private void close(Socket connection) {
        try {
            connection.close();
        } catch (IOException e) {
            this.logger.error(e);
        }
    }

    private class HttpRequestHandler implements Runnable {
        private final HttpContext context;

        HttpRequestHandler(HttpContext context) {
            this.context = context;
        }

        @Override
        public void run() {
            try {
                processor.process(context);
                if (context.getResponse() == null) {
                    throw new Exception("The HTTP processor did not produce a HTTP response");
                }
            } catch (Exception e) {
                logger.error(e);
                HttpResponse response = HttpResponseFactory.INSTANCE.create(HttpStatusCode.INTERNAL_SERVER_ERROR);
                logger.info(HttpMessageFormatter.INSTANCE.format(response));
                context.setResponse(response);
            }

            context.complete();
        }
    }

    private class PersistentConnectionResponseSender implements Runnable {
        private final HttpResponseWriter writer;
        private final PersistentConnectionRequestQueue queue;
        private Exception exception;

        private PersistentConnectionResponseSender(HttpResponseWriter writer, PersistentConnectionRequestQueue queue) {
            this.writer = writer;
            this.queue = queue;
        }

        @Override
        public void run() {
            try {
                this.runInternal();
            } catch (Exception e) {
                this.exception = e;
            }
        }

        private void runInternal() throws Exception {
            while (this.queue.waitForElement()) {
                HttpContext context = this.queue.remove();
                context.waitForCompletion();
                sendResponse(this.writer, context);
            }
        }

        private boolean hasException() {
            return this.exception != null;
        }

        private Exception getException() {
            return this.exception;
        }
    }
}
