package com.codemerx.webserver.http.handler;

import com.codemerx.webserver.Handler;
import com.codemerx.webserver.http.HttpProcessor;
import com.codemerx.webserver.http.middlewares.Middleware;
import com.codemerx.webserver.http.reader.*;
import com.codemerx.webserver.http.writer.*;
import com.codemerx.webserver.logging.*;

import java.util.*;

public class HttpHandlerBuilder {
    private int requestHandlers = Runtime.getRuntime().availableProcessors();
    private int connectionTimeout = 10;
    private List<Middleware> middlewares;
    private Logger logger = new NoopLogger();
    private HttpRequestReaderFactory requestReaderFactory = new DefaultHttpRequestReaderFactory();
    private HttpResponseWriterFactory responseWriterFactory = new DefaultHttpResponseWriterFactory();

    private HttpHandlerBuilder() {
        this.middlewares = new ArrayList<>();
    }

    public static HttpHandlerBuilder initialize() {
        return new HttpHandlerBuilder();
    }

    public HttpHandlerBuilder use(Middleware middleware) {
        this.middlewares.add(middleware);
        return this;
    }

    public HttpHandlerBuilder withRequestHandlers(int requestHandlers) {
        if (requestHandlers < 1) {
            throw new IllegalArgumentException("requestHandlers should be in the range [1,)");
        }
        this.requestHandlers = requestHandlers;
        return this;
    }

    public HttpHandlerBuilder withConnectionTimeout(int connectionTimeout) {
        if (connectionTimeout < 0) {
            throw new IllegalArgumentException("connectionTimeout should be in the range [0,)");
        }
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public HttpHandlerBuilder with(Logger logger) {
        this.logger = logger;
        return this;
    }

    public HttpHandlerBuilder with(HttpRequestReaderFactory requestReader) {
        this.requestReaderFactory = requestReader;
        return this;
    }

    public HttpHandlerBuilder with(HttpResponseWriterFactory responseWriter) {
        this.responseWriterFactory = responseWriter;
        return this;
    }

    public Handler build() {
        HttpProcessor currentProcessor = null;
        for (int i = this.middlewares.size() - 1; i >= 0; i--) {
            final Middleware currentMiddleware = this.middlewares.get(i);
            final HttpProcessor nextProcessor = currentProcessor;
            currentProcessor = context -> currentMiddleware.invoke(context, nextProcessor);
        }

        return new HttpHandler(currentProcessor, this.requestHandlers, this.connectionTimeout, this.logger, this.requestReaderFactory, this.responseWriterFactory);
    }
}
