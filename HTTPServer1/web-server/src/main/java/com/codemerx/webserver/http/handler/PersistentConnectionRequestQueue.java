package com.codemerx.webserver.http.handler;

import java.util.concurrent.ConcurrentLinkedQueue;

class PersistentConnectionRequestQueue extends ConcurrentLinkedQueue<HttpContext> {
    private boolean isCompleted = false;

    synchronized boolean isCompleted() {
        return this.isCompleted;
    }

    synchronized void complete() {
        this.isCompleted = true;
        this.notify();
    }

    /**
     * Blocks the current thread until either there is at least one element
     * in the queue or the queue is completed by calling {@link #complete()};
     * @return true if there is at least 1 element available. false if the
     * queue is completed and there are no elements in the queue.
     */
    synchronized boolean waitForElement() {
        while (this.isEmpty() && !this.isCompleted()) {
            try {
                this.wait();
            } catch (InterruptedException ignored) { }
        }

        return !this.isEmpty();
    }

    @Override
    public synchronized boolean add(HttpContext context) {
        boolean result = super.add(context);
        this.notify();
        return result;
    }
}
