package com.codemerx.webserver.http.handler;

import com.codemerx.webserver.http.types.*;

class ReadRequestResult {
    final HttpRequest request;
    final HttpResponse errorResponse;

    ReadRequestResult(HttpRequest request, HttpResponse errorResponse) {
        this.request = request;
        this.errorResponse = errorResponse;
    }
}
