package com.codemerx.webserver.http.middlewares;

import com.codemerx.webserver.http.*;
import com.codemerx.webserver.http.handler.HttpContext;
import com.codemerx.webserver.http.types.HttpResponse;
import com.codemerx.webserver.logging.Logger;

public class LoggingMiddleware implements Middleware {
    private Logger logger;

    public LoggingMiddleware(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void invoke(HttpContext context, HttpProcessor next) throws Exception {
        logger.info(HttpMessageFormatter.INSTANCE.format(context.getRequest()));
        if (next != null) {
            next.process(context);
        }
        HttpResponse response = context.getResponse();
        if (response != null) {
            logger.info(HttpMessageFormatter.INSTANCE.format(response));
        }
    }
}
