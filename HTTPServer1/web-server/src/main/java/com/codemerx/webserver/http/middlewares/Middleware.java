package com.codemerx.webserver.http.middlewares;

import com.codemerx.webserver.http.handler.HttpContext;
import com.codemerx.webserver.http.HttpProcessor;

public interface Middleware {
    void invoke(HttpContext context, HttpProcessor next) throws Exception;
}
