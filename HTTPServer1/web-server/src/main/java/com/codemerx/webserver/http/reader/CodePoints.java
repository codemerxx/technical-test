package com.codemerx.webserver.http.reader;

public class CodePoints {
    public static final int CR = 13;
    public static final int LF = 10;
    public static final int SP = 32;
    public static final int COLON = 58;
}
