package com.codemerx.webserver.http.reader;

import com.codemerx.webserver.http.types.*;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

class DefaultHttpRequestReader implements HttpRequestReader {
    private static final int EOF = -1;

    private final BufferedReader reader;

    DefaultHttpRequestReader(InputStream inputStream) {
        this.reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.US_ASCII));
    }

    @Override
    public HttpRequest read() throws IOException, HttpRequestParseException, HttpMethodNotImplementedException, HttpVersionNotSupportedException, URISyntaxException {
        HttpRequestLine requestLine = this.readRequestLine();
        if (requestLine == null) {
            return null;
        }
        HttpRequest request = new HttpRequest(requestLine);
        List<HttpHeader> headers = this.readHeaders();
        request.addHeaders(headers);
        if (request.hasHeader(HttpHeaderName.CONTENT_LENGTH)) {
            int contentLength;
            try {
                contentLength = Integer.parseInt(request.getHeader(HttpHeaderName.CONTENT_LENGTH));
            } catch (NumberFormatException ignored) {
                throw new HttpRequestParseException("The value of the Content-Length header is not a valid integer");
            }
            if (contentLength > 0) {
                byte[] body = this.readBody(contentLength);
                request.setBody(body);
            }
        }

        return request;
    }

    private HttpRequestLine readRequestLine() throws IOException, HttpRequestParseException, HttpMethodNotImplementedException, HttpVersionNotSupportedException, URISyntaxException {
        String[] parts = new String[3];
        int currentPart = 0;

        StringBuilder stringBuilder = new StringBuilder();
        int codePoint = this.reader.read();
        while (true) {
            if (codePoint == EOF) {
                if (currentPart == 0 && stringBuilder.length() == 0) {
                    return null;
                }

                throw new HttpRequestParseException("Unexpected end of request");
            }

            if (codePoint == CodePoints.CR) {
                codePoint = this.reader.read();
                if (codePoint == CodePoints.LF) {
                    parts[currentPart++] = stringBuilder.toString();
                    break;
                } else {
                    throw new HttpRequestParseException("Got CR without LF");
                }
            } else if (codePoint == CodePoints.SP) {
                if (stringBuilder.length() == 0) {
                    throw new HttpRequestParseException("Got empty request line part");
                }
                parts[currentPart++] = stringBuilder.toString();
                stringBuilder = new StringBuilder();
            } else {
                stringBuilder.appendCodePoint(codePoint);
            }

            codePoint = this.reader.read();
        }

        if (currentPart != 3) {
            throw new HttpRequestParseException("Invalid request line format");
        }

        return this.composeHttpRequestLine(parts);
    }

    private HttpRequestLine composeHttpRequestLine(String[] parts) throws HttpMethodNotImplementedException, HttpVersionNotSupportedException, URISyntaxException {
        HttpMethod method = HttpMethod.tryParse(parts[0]);
        if (method == null) {
            throw new HttpMethodNotImplementedException(parts[0]);
        }

        URI uri = new URI(parts[1]);

        HttpVersion version = HttpVersion.tryParse(parts[2]);
        if (version == null) {
            throw new HttpVersionNotSupportedException(parts[2]);
        }

        return new HttpRequestLine(method, uri, version);
    }

    private List<HttpHeader> readHeaders() throws IOException, HttpRequestParseException {
        ArrayList<HttpHeader> headers = new ArrayList<HttpHeader>();
        while (true) {
            String[] parts = this.readHeaderParts();
            if (parts == null) {
                break;
            }

            HttpHeaderName headerName = HttpHeaderName.tryParse(parts[0]);
            if (headerName == null) {
                continue;
            }

            headers.add(new HttpHeader(headerName, parts[1].trim()));
        }

        return headers;
    }

    private String[] readHeaderParts() throws IOException, HttpRequestParseException {
        String[] parts = new String[2];
        int currentPart = 0;

        StringBuilder stringBuilder = new StringBuilder();
        int codePoint = this.reader.read();
        while (true) {
            if (codePoint == EOF) {
                throw new HttpRequestParseException("Unexpected end of request");
            }

            if (codePoint == CodePoints.CR) {
                codePoint = this.reader.read();
                if (codePoint == CodePoints.LF) {
                    if (currentPart == 0 && stringBuilder.length() == 0) {  /* CRLF line */
                        return null;
                    } else {
                        parts[currentPart++] = stringBuilder.toString();
                        break;
                    }
                } else {
                    throw new HttpRequestParseException("Got CR without LF");
                }
            } else if (codePoint == CodePoints.COLON && currentPart == 0) {
                parts[currentPart++] = stringBuilder.toString();
                stringBuilder = new StringBuilder();
            } else {
                stringBuilder.appendCodePoint(codePoint);
            }

            codePoint = this.reader.read();
        }

        if (currentPart != 2) {
            throw new HttpRequestParseException("Invalid header line format");
        }

        return parts;
    }

    private byte[] readBody(int contentLength) throws IOException, HttpRequestParseException {
        byte[] body = new byte[contentLength];
        for (int i = 0; i < contentLength; i++) {
            int value = this.reader.read();
            if (value == EOF) {
                throw new HttpRequestParseException("Unexpected end of request");
            }

            body[i] = (byte)value;
        }

        return body;
    }
}
