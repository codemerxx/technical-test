package com.codemerx.webserver.http.reader;

import java.io.InputStream;

public class DefaultHttpRequestReaderFactory implements HttpRequestReaderFactory {
    @Override
    public HttpRequestReader create(InputStream inputStream) {
        return new DefaultHttpRequestReader(inputStream);
    }
}
