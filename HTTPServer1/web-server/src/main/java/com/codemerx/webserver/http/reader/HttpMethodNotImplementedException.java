package com.codemerx.webserver.http.reader;

public class HttpMethodNotImplementedException extends Exception {
    public HttpMethodNotImplementedException(String method) {
        super("HTTP Method not supported - " + method);
    }
}
