package com.codemerx.webserver.http.reader;

public class HttpRequestParseException extends Exception {
    public HttpRequestParseException(String message) {
        super("Bad request - " + message);
    }
}
