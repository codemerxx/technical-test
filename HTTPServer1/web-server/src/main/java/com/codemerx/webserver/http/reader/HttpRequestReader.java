package com.codemerx.webserver.http.reader;

import com.codemerx.webserver.http.types.HttpRequest;

import java.io.IOException;
import java.net.*;

public interface HttpRequestReader {
    HttpRequest read()
            throws IOException, HttpRequestParseException, HttpMethodNotImplementedException, HttpVersionNotSupportedException, URISyntaxException;
}
