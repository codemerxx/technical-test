package com.codemerx.webserver.http.reader;

import com.codemerx.webserver.http.types.Factory;

import java.io.InputStream;

public interface HttpRequestReaderFactory extends Factory<HttpRequestReader, InputStream> {
}
