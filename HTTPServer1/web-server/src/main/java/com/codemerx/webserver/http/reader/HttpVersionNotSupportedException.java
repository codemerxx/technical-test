package com.codemerx.webserver.http.reader;

public class HttpVersionNotSupportedException extends Exception {
    public HttpVersionNotSupportedException(String version) {
        super("HTTP Version not supported - " + version);
    }
}
