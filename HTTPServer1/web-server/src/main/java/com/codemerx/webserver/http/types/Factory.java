package com.codemerx.webserver.http.types;

public interface Factory<T, K> {
    T create(K arg);
}
