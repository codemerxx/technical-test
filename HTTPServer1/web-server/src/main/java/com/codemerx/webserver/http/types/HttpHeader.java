package com.codemerx.webserver.http.types;

public class HttpHeader {
    private final HttpHeaderName name;
    private final String value;

    public HttpHeader(HttpHeaderName name, String value) {
        this.name = name;
        this.value = value;
    }

    public HttpHeaderName getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }
}
