package com.codemerx.webserver.http.types;

public class HttpHeaderValues {
    public static final String CLOSE = "Close";
    public static final String KEEP_ALIVE = "Keep-Alive";

    private HttpHeaderValues() { }
}
