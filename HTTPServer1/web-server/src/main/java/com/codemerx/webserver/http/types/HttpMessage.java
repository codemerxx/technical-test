package com.codemerx.webserver.http.types;

import java.util.*;
import java.util.stream.Collectors;

class HttpMessage<T> {
    private final Map<HttpHeaderName, String> headers;
    private T body;

    protected HttpMessage() {
        this.headers = new HashMap<>();
    }

    public boolean hasHeader(HttpHeaderName header) {
        this.validateHeader(header);
        return this.headers.containsKey(header);
    }

    public String getHeader(HttpHeaderName header) {
        this.validateHeader(header);
        return this.headers.get(header);
    }

    public void addHeader(HttpHeaderName header, String value) {
        this.validateHeader(header);
        if (value == null) {
            throw new IllegalArgumentException("value cannot be null");
        }

        this.headers.put(header, value);
    }

    public void addHeaders(List<HttpHeader> headers) {
        if (headers == null) {
            throw new IllegalArgumentException("headers cannot be null");
        }

        for (HttpHeader header: headers) {
            this.addHeader(header.getName(), header.getValue());
        }
    }

    public List<HttpHeader> getHeaders() {
        return this.headers.entrySet()
                           .stream()
                           .map(entry -> new HttpHeader(entry.getKey(), entry.getValue()))
                           .collect(Collectors.toList());
    }

    public boolean hasBody() {
        return this.body != null;
    }

    public T getBody() {
        return this.body;
    }

    public void setBody(T body) {
        if (body == null) {
            throw new IllegalArgumentException("body cannot be null");
        }

        this.body = body;
    }

    private void validateHeader(HttpHeaderName header) {
        if (header == null) {
            throw new IllegalArgumentException("header cannot be null");
        }
    }
}
