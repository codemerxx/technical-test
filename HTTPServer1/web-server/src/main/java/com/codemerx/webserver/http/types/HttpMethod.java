package com.codemerx.webserver.http.types;

public enum HttpMethod {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE;

    public static HttpMethod tryParse(String string) {
        try {
            return HttpMethod.valueOf(string);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
