package com.codemerx.webserver.http.types;

public class HttpRequest extends HttpMessage<byte[]> {
    private final HttpRequestLine requestLine;

    public HttpRequest(HttpRequestLine requestLine) {
        this.requestLine = requestLine;
    }

    public HttpRequestLine getRequestLine() {
        return this.requestLine;
    }
}
