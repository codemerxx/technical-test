package com.codemerx.webserver.http.types;

import java.net.URI;

public class HttpRequestLine {
    private final HttpMethod method;
    private final URI uri;
    private final HttpVersion httpVersion;

    public HttpRequestLine(HttpMethod method, URI uri, HttpVersion httpVersion) {
        this.method = method;
        this.uri = uri;
        this.httpVersion = httpVersion;
    }

    public HttpMethod getMethod() {
        return this.method;
    }

    public URI getUri() {
        return this.uri;
    }

    public HttpVersion getHttpVersion() {
        return this.httpVersion;
    }
}
