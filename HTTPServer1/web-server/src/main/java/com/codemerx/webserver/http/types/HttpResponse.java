package com.codemerx.webserver.http.types;

import java.io.InputStream;

public class HttpResponse extends HttpMessage<InputStream> {
    private final HttpStatusLine statusLine;

    public HttpResponse(HttpStatusLine statusLine) {
        this.statusLine = statusLine;
    }

    public HttpStatusLine getStatusLine() {
        return this.statusLine;
    }
}
