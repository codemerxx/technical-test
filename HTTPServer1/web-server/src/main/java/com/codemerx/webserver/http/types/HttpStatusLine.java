package com.codemerx.webserver.http.types;

public class HttpStatusLine {
    private final HttpVersion httpVersion;
    private final HttpStatusCode statusCode;

    public HttpStatusLine(HttpVersion httpVersion, HttpStatusCode statusCode) {
        this.httpVersion = httpVersion;
        this.statusCode = statusCode;
    }

    public HttpVersion getHttpVersion() {
        return this.httpVersion;
    }

    public HttpStatusCode getStatusCode() {
        return this.statusCode;
    }
}
