package com.codemerx.webserver.http.types;

import java.util.Objects;

public enum HttpVersion {
    HTTPv0_9("HTTP/0.9"),
    HTTPv1_0("HTTP/1.0"),
    HTTPv1_1("HTTP/1.1");

    private final String displayName;

    private HttpVersion(String displayName) {
        this.displayName = displayName;
    }

    public static HttpVersion tryParse(String string) {
        for (HttpVersion version: HttpVersion.values()) {
            if (Objects.equals(string, version.displayName)) {
                return version;
            }
        }

        return null;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
