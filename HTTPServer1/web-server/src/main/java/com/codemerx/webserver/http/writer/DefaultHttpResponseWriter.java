package com.codemerx.webserver.http.writer;

import com.codemerx.webserver.http.reader.*;
import com.codemerx.webserver.http.types.*;

import java.io.*;
import java.nio.charset.StandardCharsets;

class DefaultHttpResponseWriter implements HttpResponseWriter {
    private final OutputStream outputStream;
    private final OutputStreamWriter writer;

    DefaultHttpResponseWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
        this.writer = new OutputStreamWriter(this.outputStream, StandardCharsets.US_ASCII);
    }

    @Override
    public void write(HttpResponse response) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        this.writeStatusLine(stringBuilder, response.getStatusLine());
        this.writeEndOfLine(stringBuilder);
        for(HttpHeader header: response.getHeaders()) {
            this.writeHeader(stringBuilder, header);
            this.writeEndOfLine(stringBuilder);
        }

        this.writeEndOfLine(stringBuilder);

        this.writer.write(stringBuilder.toString());
        this.writer.flush();

        if (response.hasBody()) {
            try (InputStream body = response.getBody()) {
                body.transferTo(this.outputStream);
                this.outputStream.flush();
            }
        }
    }

    private void writeEndOfLine(StringBuilder stringBuilder) {
        stringBuilder.appendCodePoint(CodePoints.CR);
        stringBuilder.appendCodePoint(CodePoints.LF);
    }

    private void writeStatusLine(StringBuilder stringBuilder, HttpStatusLine statusLine) {
        stringBuilder.append(statusLine.getHttpVersion().getDisplayName());
        stringBuilder.appendCodePoint(CodePoints.SP);
        stringBuilder.append(statusLine.getStatusCode().getCode());
        stringBuilder.appendCodePoint(CodePoints.SP);
        stringBuilder.append(statusLine.getStatusCode().getReasonPhrase());
    }

    private void writeHeader(StringBuilder stringBuilder, HttpHeader header) {
        stringBuilder.append(header.getName().getDisplayName());
        stringBuilder.appendCodePoint(CodePoints.COLON);
        stringBuilder.appendCodePoint(CodePoints.SP);
        stringBuilder.append(header.getValue());
    }
}
