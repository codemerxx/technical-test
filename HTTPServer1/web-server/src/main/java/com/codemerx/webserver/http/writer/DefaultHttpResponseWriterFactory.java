package com.codemerx.webserver.http.writer;

import java.io.OutputStream;

public class DefaultHttpResponseWriterFactory implements HttpResponseWriterFactory {
    @Override
    public HttpResponseWriter create(OutputStream outputStream) {
        return new DefaultHttpResponseWriter(outputStream);
    }
}
