package com.codemerx.webserver.http.writer;

import com.codemerx.webserver.http.types.HttpResponse;

import java.io.IOException;

public interface HttpResponseWriter {
    void write(HttpResponse response) throws IOException;
}
