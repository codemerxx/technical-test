package com.codemerx.webserver.http.writer;

import com.codemerx.webserver.http.types.Factory;

import java.io.OutputStream;

public interface HttpResponseWriterFactory extends Factory<HttpResponseWriter, OutputStream> {
}
