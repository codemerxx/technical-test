package com.codemerx.webserver.logging;

public class ConsoleLogger implements Logger {
    private Formatter formatter;

    public ConsoleLogger(Formatter formatter) {
        if (formatter == null) {
            throw new IllegalArgumentException("formatter cannot be null");
        }
        this.formatter = formatter;
    }

    @Override
    public void info(String message) {
        System.out.println(this.formatter.formatLog(message));
    }

    @Override
    public void error(Exception exception) {
        System.out.println(this.formatter.formatError(exception));
    }

    @Override
    public void error(String message) {
        System.out.println(this.formatter.formatError(message));
    }

    @Override
    public void warning(String message) {
        System.out.println(this.formatter.formatWarning(message));
    }
}
