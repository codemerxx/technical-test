package com.codemerx.webserver.logging;

import java.io.*;

public class DebugFormatter extends DefaultFormatter {
    @Override
    public String formatError(Exception exception) {
        return prependWithDate("ERROR: " + getStackTrace(exception));
    }

    private static String getStackTrace(Exception exception) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        return stringWriter.toString();
    }
}
