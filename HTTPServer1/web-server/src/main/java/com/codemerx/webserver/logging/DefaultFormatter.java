package com.codemerx.webserver.logging;

import java.time.format.DateTimeFormatter;

public class DefaultFormatter implements Formatter {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public String formatLog(String message) {
        return prependWithDate("INFO: " + message);
    }

    @Override
    public String formatError(Exception exception) {
        return prependWithDate("ERROR: " + exception.toString());
    }

    @Override
    public String formatError(String message) {
        return prependWithDate("ERROR: " + message);
    }

    @Override
    public String formatWarning(String message) {
        return prependWithDate("WARNING: " + message);
    }

    protected static String prependWithDate(String message) {
        return java.time.LocalDateTime.now().format(formatter) + " " + message;
    }
}
