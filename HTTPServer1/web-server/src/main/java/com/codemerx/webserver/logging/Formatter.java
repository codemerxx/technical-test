package com.codemerx.webserver.logging;

public interface Formatter {
    String formatLog(String message);
    String formatError(Exception exception);
    String formatError(String message);
    String formatWarning(String message);
}
