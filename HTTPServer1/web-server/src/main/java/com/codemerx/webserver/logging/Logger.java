package com.codemerx.webserver.logging;

public interface Logger {
    void info(String message);
    void error(Exception message);
    void error(String message);
    void warning(String message);
}
