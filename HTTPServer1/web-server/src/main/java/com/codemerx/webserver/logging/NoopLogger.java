package com.codemerx.webserver.logging;

public class NoopLogger implements Logger {
    @Override
    public void info(String message) { }

    @Override
    public void error(Exception message) { }

    @Override
    public void error(String message) { }

    @Override
    public void warning(String message) { }
}
