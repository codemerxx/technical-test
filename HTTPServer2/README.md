# File Based Web Server

HTTP web server application for serving files residing under a given root folder. The root folder is the folder the server is started from. The server supports HTTP/1.1 Keep-Alive connections.

# Prerequisites

- Java 12 or higher

# Build and Run

## Windows

```bash
gradlew build
java -jar build\libs\webserver-2.0-SNAPSHOT.jar --port=3456 --maxPoolSize=10 --maxKeepAlivePoolSize=64 --keepAliveConnectionTimeout=10
```
## UNIX
```bash
sh gradlew build
java -jar ./build/libs/webserver-2.0-SNAPSHOT.jar --port=3456 --maxPoolSize=10 --maxKeepAlivePoolSize=64 --keepAliveConnectionTimeout=10
```

# Precompiled JAR

A precompiled copy of the server can be found in the ```./binaries``` folder and can be started like this:

## Windows

```bash
java -jar binaries\webserver-2.0-SNAPSHOT.jar
```
## UNIX
```bash
java -jar ./binaries/webserver-2.0-SNAPSHOT.jar
```

# Command-line arguments

The server executable accepts the following arguments:
- `--port=<port-number>` - sets the port number to serve on. Default value is *3456*.
- `--maxPoolSize=<pool-size>` - sets the maximum thread pool size for serving requests. Default value is *10*.
- `--maxKeepAlivePoolSize=<pool-size>` - sets the maximum thread pool size for serving requests of a single Keep-Alive connection. Default value is *64*.
- `--keepAliveConnectionTimeout=<timeout>` - sets the maximum time in seconds Keep-Alive connections should wait for a request. Default value is *10* seconds.
- `--verbose` - turns on verbose logging.

# Server Architecture

The ```WebServer``` class is the main requests serving point. It executes a pipeline of handlers. All handlers share an instance of  *ApplicationContext* which contains the base application configuration. Any number of handlers can be composed and passed to the *WebServer* instance:

```java
...
LoggingHttpHandler loggingHttpHandler = new LoggingHttpHandler(errorHttpHandler);

BasicHttpHandler basicHttpHandler = new BasicHttpHandler(factory, loggingHttpHandler);

WebServer webServer = new WebServer(this.applicationContext, serverSocket, executorService, basicHttpHandler);
```

This modular architecture allows easily adding custom handlers and extending the server functionality. 