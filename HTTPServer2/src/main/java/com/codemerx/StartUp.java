package com.codemerx;

import com.codemerx.factories.Log4JLoggerFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.ArgumentsParser;
import com.codemerx.utils.Logger;
import com.codemerx.web.Config;
import com.codemerx.web.FileWebServer;

import java.io.IOException;
import java.nio.file.Path;

public class StartUp {
    private static ArgumentsParser parser = new ArgumentsParser();

    public static void main(String[] args) {
        final ArgumentsParser.Arguments arguments = parser.parse(args);

        final ApplicationContext applicationContext = buildApplicationContext(arguments);
        final FileWebServer fileWebServer = new FileWebServer(applicationContext);

        final Logger logger = applicationContext.getLoggerFactory().createLogger(StartUp.class);
        logger.info(String.format("Listening on port %d, with Maximum Pool Size of %d and maximum Keep-Alive Pool Size of %d.", arguments.getPort(), arguments.getMaxPoolSize(), arguments.getMaxKeepAliveConnectionsPoolSize()));
        logger.info("Serving folder: " + applicationContext.getConfig().getRootFolder());

        try {
            fileWebServer.listen(arguments.getPort());
        } catch (IOException e) {
            logger.error("Error while starting the server: " + e.getMessage());
        }
    }

    private static ApplicationContext buildApplicationContext(final ArgumentsParser.Arguments arguments) {
        String userDir = System.getProperty("user.dir");
        Path path = Path.of(userDir);
        String rootFolderPath = path.toString();
        Config config = new Config(rootFolderPath, arguments.getMaxPoolSize(), arguments.getMaxKeepAliveConnectionsPoolSize(), arguments.getKeepAliveConnectionTimeout());

        return new ApplicationContext(config, new Log4JLoggerFactory(arguments.getVerbose()));
    }
}
