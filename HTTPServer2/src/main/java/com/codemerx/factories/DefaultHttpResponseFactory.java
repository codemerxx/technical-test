package com.codemerx.factories;

import com.codemerx.http.*;

public class DefaultHttpResponseFactory implements HttpResponseFactory {
    private static final String HTTP = "HTTP";

    @Override
    public HttpResponse createResponse(final HttpStatus status) {
        return new HttpResponse(new HttpStatusLine(new Protocol(HTTP, 1, 1), status));
    }
}
