package com.codemerx.factories;

import com.codemerx.http.*;

public interface HttpResponseFactory {
    HttpResponse createResponse(HttpStatus status);
}
