package com.codemerx.factories;

import org.apache.log4j.PropertyConfigurator;
import com.codemerx.utils.Log4JLogger;
import com.codemerx.utils.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

public class Log4JLoggerFactory implements LoggerFactory {
    private final boolean isVerbose;

    static {
        Path rootDirectory = null;
        try {
            rootDirectory = new File(Log4JLoggerFactory.class.getProtectionDomain().getCodeSource().getLocation().toURI()).toPath().getParent();
        } catch (URISyntaxException ignored) { }

        PropertyConfigurator.configure(rootDirectory.resolve("log4j.properties").toString());
    }

    public Log4JLoggerFactory(boolean isVerbose) {
        this.isVerbose = isVerbose;
    }

    @Override
    public Logger createLogger(Class clazz) {
        return new Log4JLogger(clazz, this.isVerbose);
    }
}
