package com.codemerx.factories;

import com.codemerx.utils.Logger;

public interface LoggerFactory {
    Logger createLogger(Class clazz);
}
