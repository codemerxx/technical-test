package com.codemerx.http;

public class BasicHttpEntity {
    private byte[] content;

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public int getContentLength() {
        return this.content.length;
    }
}
