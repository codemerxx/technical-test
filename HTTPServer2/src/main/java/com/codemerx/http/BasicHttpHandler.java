package com.codemerx.http;

import com.codemerx.factories.HttpResponseFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class BasicHttpHandler implements Handler<Socket, Void> {
    private static final String DATE_FORMAT = "EEE, dd MM yyyy hh:mm:ss X";
    private static final String CONNECTION = "Connection";
    private static final String KEEP_ALIVE = "keep-alive";
    private static final String SERVER = "Server";
    private static final String DATE = "Date";
    public static final String CODE_MERX = "CodeMerx";
    public static final String CLOSE = "Close";

    private final Handler<HttpRequest, HttpResponse> httpHandler;
    private final HttpResponseFactory httpResponseFactory;

    public BasicHttpHandler(HttpResponseFactory httpResponseFactory, Handler<HttpRequest, HttpResponse> httpHandler) {
        this.httpHandler = httpHandler;
        this.httpResponseFactory = httpResponseFactory;
    }

    @Override
    public Void handle(final Socket socket, final ApplicationContext applicationContext) throws Throwable {
        final Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
        final InputStream inputStream = socket.getInputStream();
        final OutputStream outputStream = socket.getOutputStream();

        final HttpRequest httpRequest;
        try {
            httpRequest = this.parseHttpRequest(inputStream);
            final boolean isKeepAlive = httpRequest.getHeaders()
                    .stream()
                    .filter(h -> h.getName().equals(CONNECTION))
                    .anyMatch(h -> h.getValue().toLowerCase().equals(KEEP_ALIVE));

            if (isKeepAlive) {
                this.handleKeepAliveRequests(socket, applicationContext, logger, inputStream, outputStream, httpRequest);
            } else {
                logger.verbose("Non Keep-Alive request.");
                final HttpResponse httpResponse = this.httpHandler.handle(httpRequest, applicationContext);
                this.writeHttpResponseToStream(outputStream, httpResponse, false);
            }
        } catch (IOException e) {
            this.sendResponseWithErrorCode(outputStream, HttpStatus.INTERNAL_SERVER_ERROR, logger, e.getMessage());
            return null;
        } catch (HttpRequestFormatException e) {
            this.sendResponseWithErrorCode(outputStream, HttpStatus.BAD_REQUEST, logger, e.getMessage());
            return null;
        }

        return null;
    }

    private void handleKeepAliveRequests(final Socket socket,
                                         final ApplicationContext applicationContext,
                                         final Logger logger,
                                         final InputStream inputStream,
                                         final OutputStream outputStream,
                                         final HttpRequest httpRequest
    ) throws IOException, ConnectionClosedException, HttpRequestFormatException, ExecutionException, InterruptedException {

        socket.setSoTimeout(applicationContext.getConfig().getKeepAliveTimeout() * 1000);

        final ExecutorService responseGenerationExecutor = Executors.newFixedThreadPool(applicationContext.getConfig().getMaxKeepAliveConnectionsPoolSize());
        final ExecutorService sendResponseExecutor = Executors.newFixedThreadPool(applicationContext.getConfig().getMaxSendKeepAliveResponsesPoolSize());

        final ProcessingGuard<HttpResponse> generateKeepAliveResponseGuard = new ProcessingGuard<>();
        final ProcessingGuard<Boolean> sendResponseGuard = new ProcessingGuard<>();

        final CompletableFuture<ProcessingResult<HttpResponse>> generateInitialResponseFuture = CompletableFuture.supplyAsync(
                () -> generateKeepAliveResponseGuard.guard(() -> {
                    logger.verbose("Socket: " + socket.getPort() + " Initial request: " + Thread.currentThread().getName());
                    return this.httpHandler.handle(httpRequest, applicationContext);
                }), responseGenerationExecutor);

        CompletableFuture<ProcessingResult<Boolean>> sendResponsePipeline = CompletableFuture.supplyAsync(
                () -> sendResponseGuard.guard(
                        () -> this.sendKeepAliveResponseCallback(socket, logger, outputStream, generateInitialResponseFuture, " Initial response: ")
                ), sendResponseExecutor);

        while (true) {
            final HttpRequest keepAliveHttpRequest;
            try {
                keepAliveHttpRequest = this.parseHttpRequest(inputStream);
                logger.verbose("Socket: " + socket.getPort() + " Request with thread: " + Thread.currentThread().getName());
            } catch (SocketTimeoutException e) {
                break;
            }

            if (sendResponsePipeline.isCompletedExceptionally()) {
                break;
            }

            final CompletableFuture<ProcessingResult<HttpResponse>> generateKeepAliveResponseFuture = CompletableFuture.supplyAsync(
                    () -> generateKeepAliveResponseGuard.guard(
                            () -> this.httpHandler.handle(keepAliveHttpRequest, applicationContext)
                    ), responseGenerationExecutor);

            sendResponsePipeline = sendResponsePipeline.thenComposeAsync(processingResult -> {
                if (!processingResult.result) {
                    CompletableFuture<ProcessingResult<Boolean>> result = new CompletableFuture<>();
                    result.completeExceptionally(processingResult.exception);
                    return result;
                }

                return CompletableFuture.supplyAsync(
                        () -> sendResponseGuard.guard(
                                () -> this.sendKeepAliveResponseCallback(socket, logger, outputStream, generateKeepAliveResponseFuture, " Response with thread: ")
                        ), sendResponseExecutor);
            });
        }

        sendResponsePipeline.get();
        logger.verbose("Connection closed with id: " + socket.getPort());
    }

    private Boolean sendKeepAliveResponseCallback(final Socket socket,
                                                  final Logger logger,
                                                  final OutputStream outputStream,
                                                  final CompletableFuture<ProcessingResult<HttpResponse>> generateResponseFuture,
                                                  final String debugMessage) throws Throwable {
        final ProcessingResult<HttpResponse> generateResponseResult = generateResponseFuture.get();
        if (generateResponseResult.exception != null) {
            this.sendResponseWithErrorCode(outputStream, HttpStatus.INTERNAL_SERVER_ERROR, logger, generateResponseResult.exception.getMessage());
            throw generateResponseResult.exception;
        }

        logger.verbose("Socket: " + socket.getPort() + debugMessage + Thread.currentThread().getName());
        this.writeHttpResponseToStream(outputStream, generateResponseResult.result, true);
        return true;
    }

    private HttpRequest parseHttpRequest(final InputStream inputStream) throws IOException, ConnectionClosedException, HttpRequestFormatException {
        DefaultHttpRequestParser defaultHttpRequestParser = new DefaultHttpRequestParser(inputStream);

        return defaultHttpRequestParser.parse();
    }

    private void writeHttpResponseToStream(final OutputStream outputStream,
                                           final HttpResponse httpResponse,
                                           final boolean isKeepAlive) throws ConnectionClosedException {
        if (isKeepAlive) {
            httpResponse.addHeader(new HttpHeader(CONNECTION, KEEP_ALIVE));
        } else {
            httpResponse.addHeader(new HttpHeader(CONNECTION, CLOSE));
        }

        httpResponse.addHeader(new HttpHeader(SERVER, CODE_MERX));
        String dateHeader = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        httpResponse.addHeader(new HttpHeader(DATE, dateHeader));

        DefaultHttpResponseWriter responseWriter = new DefaultHttpResponseWriter(outputStream);
        responseWriter.write(httpResponse);
    }

    private void sendResponseWithErrorCode(final OutputStream outputStream,
                                           final HttpStatus status,
                                           final Logger logger,
                                           final String errorMessage) throws ConnectionClosedException {
        logger.info("Handled exception while reading request: " + errorMessage);
        HttpResponse httpResponse = this.httpResponseFactory.createResponse(status);
        this.writeHttpResponseToStream(outputStream, httpResponse, false);

        logger.info(String.format("%s %s", status.getCode(), status.getReason()));
    }

    private static class ProcessingGuard<T> {
        private ProcessingResult<T> guard(UnsafeSupplier<T> supplier) {
            try {
                return new ProcessingResult<>(supplier.get(), null);
            } catch (Throwable e) {
                return new ProcessingResult<>(null, e);
            }
        }
    }

    @FunctionalInterface
    private interface UnsafeSupplier<T> {
        T get() throws Throwable;
    }

    private static class ProcessingResult<T> {
        private final T result;
        private final Throwable exception;

        private ProcessingResult(final T result, final Throwable exception) {
            this.result = result;
            this.exception = exception;
        }

        private T getResult() {
            return this.result;
        }

        private Throwable getException() {
            return this.exception;
        }
    }
}
