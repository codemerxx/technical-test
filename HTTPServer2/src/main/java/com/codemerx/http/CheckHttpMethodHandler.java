package com.codemerx.http;

import com.codemerx.factories.HttpResponseFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

public class CheckHttpMethodHandler implements Handler<HttpRequest, HttpResponse> {
    private final Handler<HttpRequest, HttpResponse> httpHandler;
    private final HttpResponseFactory httpResponseFactory;

    public CheckHttpMethodHandler(HttpResponseFactory httpResponseFactory, Handler<HttpRequest, HttpResponse> httpHandler) {
        this.httpHandler = httpHandler;
        this.httpResponseFactory = httpResponseFactory;
    }

    @Override
    public HttpResponse handle(final HttpRequest httpRequest, final ApplicationContext applicationContext) throws Throwable {
        final HttpResponse httpResponse;
        final String method = httpRequest.getRequestLine().getMethod();

        if (HttpMethod.GET.equals(method)) {
            httpResponse = this.httpHandler.handle(httpRequest, applicationContext);
        } else {
            HttpStatus responseStatus = HttpStatus.NOT_IMPLEMENTED;
            httpResponse = this.httpResponseFactory.createResponse(responseStatus);

            final Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
            logger.warning("Invalid request! Returning response with code: " + responseStatus.getCode());
        }

        return httpResponse;
    }
}
