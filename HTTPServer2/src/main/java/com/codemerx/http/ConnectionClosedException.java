package com.codemerx.http;

public class ConnectionClosedException extends Exception {
    public ConnectionClosedException() {
        super("Connection was closed.");
    }
}
