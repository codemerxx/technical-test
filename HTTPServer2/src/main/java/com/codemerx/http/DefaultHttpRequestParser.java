package com.codemerx.http;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

enum ParseStep {
    READ_REQUEST_LINE,
    READ_HEADERS,
    READ_ENTITY,
    DONE;
}

public class DefaultHttpRequestParser implements HttpRequestParser {
    private static final String CONTENT_LENGTH_HEADER_NAME = "Content-Length";
    private static final String PROTOCOL_NAME = "HTTP";
    private static final String COLON = ":";
    private final InputStream inputStream;

    public DefaultHttpRequestParser(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public HttpRequest parse() throws IOException, HttpRequestFormatException, ConnectionClosedException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(this.inputStream));

        ParseStep currentStep = ParseStep.READ_REQUEST_LINE;
        HttpRequest request = null;

        while(currentStep != ParseStep.DONE) {
            switch (currentStep) {
                case READ_REQUEST_LINE: {
                    final String currentLine = reader.readLine();
                    final HttpRequestLine requestLine = this.parseRequestLine(currentLine);
                    request = new HttpRequest(requestLine);

                    currentStep = ParseStep.READ_HEADERS;
                    break;
                }
                case READ_HEADERS: {
                    String currentLine = reader.readLine();

                    while(currentLine.length() > 0) {
                        final HttpHeader header = this.parseHeader(currentLine);
                        request.addHeader(header);

                        currentLine = reader.readLine();
                    }

                    currentStep = ParseStep.READ_ENTITY;
                    break;
                }
                case READ_ENTITY: {
                    final HttpHeader contentLengthHeader = request.getHeaders()
                            .stream()
                            .filter(h -> CONTENT_LENGTH_HEADER_NAME.equalsIgnoreCase(h.getName()))
                            .findFirst()
                            .orElse(null);

                    if (contentLengthHeader != null) {
                        int contentLength = Integer.parseInt(contentLengthHeader.getValue());

                        if (contentLength > 0) {
                            final BasicHttpEntity entity = new BasicHttpEntity();
                            entity.setContent(this.readEntityData(reader, contentLength));
                            request.setEntity(entity);
                        }
                    }

                    currentStep = ParseStep.DONE;
                    break;
                }
            }
        }

        return request;
    }

    private byte[] readEntityData(final BufferedReader reader, final int contentLength) throws IOException {
        final byte[] entityData = new byte[contentLength];

        for(int i = 0; i < contentLength; i++) {
            entityData[i] = (byte)reader.read();
        }

        return entityData;
    }

    private HttpRequestLine parseRequestLine(final String line) throws HttpRequestFormatException, ConnectionClosedException {
        if (line == null) {
            throw new ConnectionClosedException();
        }

        final String[] parts = line.split(" ");
        if (parts.length != 3) {
            throw new HttpRequestFormatException();
        }

        final String[] protocolParts = parts[2].split("/");
        if (protocolParts.length != 2 || !PROTOCOL_NAME.equals(protocolParts[0])) {
            throw new HttpRequestFormatException();
        }

        final String[] protocolVersionParts = protocolParts[1].split("\\.");

        if (protocolVersionParts.length != 2) {
            throw new HttpRequestFormatException();
        }

        int protocolMajorVersion;
        int protocolMinorVersion;

        try {
            protocolMajorVersion = Integer.parseInt(protocolVersionParts[0]);
            protocolMinorVersion = Integer.parseInt(protocolVersionParts[1]);
        } catch(NumberFormatException e) {
            throw new HttpRequestFormatException();
        }

        final URI uri;
        try {
            uri = new URI(parts[1]);
        } catch (URISyntaxException e) {
            throw new HttpRequestFormatException();
        }

        return new HttpRequestLine(parts[0], uri.getPath(), new Protocol(protocolParts[0], protocolMajorVersion, protocolMinorVersion));
    }

    private HttpHeader parseHeader(final String line) throws HttpRequestFormatException {
        int headerSeparatorIndex = line.indexOf(COLON);

        if (headerSeparatorIndex < 0) {
            throw new HttpRequestFormatException();
        }

        String headerName = line.substring(0, headerSeparatorIndex);
        String headerValue = line.substring(headerSeparatorIndex + 1).trim();

        return new HttpHeader(headerName, headerValue);
    }
}
