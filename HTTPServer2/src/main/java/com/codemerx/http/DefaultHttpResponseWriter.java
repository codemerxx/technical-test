package com.codemerx.http;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class DefaultHttpResponseWriter implements ResponseWriter {
    private static final String CRLF = "\r\n";
    private final OutputStream outputStream;

    public DefaultHttpResponseWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public void write(final HttpResponse response) throws ConnectionClosedException {
        final StringBuilder builder = new StringBuilder();

        builder.append(this.formatStatusLine(response.getStatusLine()));
        this.appendLine(builder);
        this.writeHeaders(builder, response.getHeaders());
        this.appendLine(builder);
        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(this.outputStream, StandardCharsets.US_ASCII));

        try {
            writer.write(builder.toString());
            writer.flush();
        } catch(IOException e) {
            throw new ConnectionClosedException();
        }

        final BasicHttpEntity responseEntity = response.getEntity();
        if (responseEntity != null && responseEntity.getContentLength() > 0) {
            try {
                this.outputStream.write(responseEntity.getContent());
                this.outputStream.flush();
            } catch(IOException e) {
                throw new ConnectionClosedException();
            }
        }
    }

    private String formatStatusLine(final HttpStatusLine statusLine) {
        final Protocol requestProtocol = statusLine.getProtocol();
        final HttpStatus responseStatus = statusLine.getStatus();
        final String formattedProtocol = String.format("%s/%d.%d", requestProtocol.getProtocolName(), requestProtocol.getMajorVersion(), requestProtocol.getMinorVersion());
        final String formattedStatusLine = String.format("%s %d %s", formattedProtocol, responseStatus.getCode(), responseStatus.getReason());

        return formattedStatusLine;
    }

    private void writeHeaders(final StringBuilder builder, final ArrayList<HttpHeader> headers) {
        for (HttpHeader header: headers) {
            builder.append(this.formatHeader(header));
            this.appendLine(builder);
        }
    }

    private String formatHeader(final HttpHeader header) {
        return String.format("%s: %s", header.getName(), header.getValue());
    }

    private void appendLine(final StringBuilder builder) {
        builder.append(CRLF);
    }
}
