package com.codemerx.http;

import com.codemerx.factories.HttpResponseFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

public class ErrorHttpHandler implements Handler<HttpRequest, HttpResponse> {
    public static final String CONTENT_LENGTH = "Content-Length";
    private final Handler<HttpRequest, HttpResponse> httpHandler;
    private final HttpResponseFactory httpResponseFactory;

    public ErrorHttpHandler(HttpResponseFactory httpResponseFactory, Handler<HttpRequest, HttpResponse> httpHandler) {
        this.httpResponseFactory = httpResponseFactory;
        this.httpHandler = httpHandler;
    }

    @Override
    public HttpResponse handle(final HttpRequest httpRequest, final ApplicationContext applicationContext) throws Throwable {
        HttpResponse httpResponse;

        try {
            httpResponse = this.httpHandler.handle(httpRequest, applicationContext);
        } catch (Exception e) {
            httpResponse = this.httpResponseFactory.createResponse(HttpStatus.INTERNAL_SERVER_ERROR);

            Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
            logger.error("Handled exception: " + e.getMessage());
        }

        if (httpResponse.getStatusLine().getStatus() != HttpStatus.OK) {
            httpResponse.addHeader(new HttpHeader(CONTENT_LENGTH, "0"));
        }

        return httpResponse;
    }
}
