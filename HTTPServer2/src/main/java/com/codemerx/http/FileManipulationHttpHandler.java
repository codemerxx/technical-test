package com.codemerx.http;

import com.codemerx.factories.HttpResponseFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;

public class FileManipulationHttpHandler implements Handler<HttpRequest, HttpResponse> {
    public static final String CONTENT_LENGTH = "Content-Length";
    private final HttpResponseFactory httpResponseFactory;

    public FileManipulationHttpHandler(HttpResponseFactory httpResponseFactory) {
        this.httpResponseFactory = httpResponseFactory;
    }

    @Override
    public HttpResponse handle(final HttpRequest httpRequest, final ApplicationContext applicationContext) {
        final Path path = Path.of(applicationContext.getConfig().getRootFolder(), httpRequest.getRequestLine().getUri());
        File file = new File(path.toString());

        if (file.isDirectory()) {
            file = Path.of(path.toString(), applicationContext.getConfig().getIndexFilePath()).toFile();
        }

        HttpResponse httpResponse;

        try (FileInputStream fileIn = new FileInputStream(file)) {
            httpResponse = this.httpResponseFactory.createResponse(HttpStatus.OK);

            byte[] data = fileIn.readAllBytes();
            httpResponse.addHeader(new HttpHeader(CONTENT_LENGTH, String.valueOf(data.length)));

            BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
            basicHttpEntity.setContent(data);

            httpResponse.setEntity(basicHttpEntity);
        } catch (IOException e) {
            httpResponse = this.httpResponseFactory.createResponse(HttpStatus.INTERNAL_SERVER_ERROR);

            Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
            logger.error("Handled exception while retrieving file: " + e.getMessage());
        }

        return httpResponse;
    }
}
