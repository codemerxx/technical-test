package com.codemerx.http;

import java.util.ArrayList;

public class HttpMessage {
    private final ArrayList<HttpHeader> headers;
    private BasicHttpEntity entity;

    public HttpMessage() {
        this.headers = new ArrayList<HttpHeader>();
    }

    public void addHeader(HttpHeader header) {
        this.headers.add(header);
    }

    public ArrayList<HttpHeader> getHeaders() {
        return this.headers;
    }

    public BasicHttpEntity getEntity() {
        return this.entity;
    }

    public void setEntity(BasicHttpEntity entity) {
        this.entity = entity;
    }
}
