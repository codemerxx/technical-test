package com.codemerx.http;

public class HttpRequest extends HttpMessage {
    private final HttpRequestLine requestLine;

    public HttpRequest(HttpRequestLine requestLine) {
        this.requestLine = requestLine;
    }

    public HttpRequestLine getRequestLine() {
        return this.requestLine;
    }
}
