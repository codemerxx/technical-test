package com.codemerx.http;

public class HttpRequestFormatException extends Exception {
    public HttpRequestFormatException() {
        super("Request was badly formatted.");
    }
}
