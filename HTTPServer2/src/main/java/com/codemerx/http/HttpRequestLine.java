package com.codemerx.http;

public class HttpRequestLine {
    private final String method;
    private final String uri;
    private final Protocol protocol;

    HttpRequestLine(String method, String uri, Protocol protocol) {
        this.method = method;
        this.uri = uri;
        this.protocol = protocol;
    }

    public String getMethod() {
        return this.method;
    }

    public String getUri() {
        return this.uri;
    }

    public Protocol getProtocol() {
        return this.protocol;
    }
}
