package com.codemerx.http;

import java.io.IOException;

public interface HttpRequestParser {
    HttpRequest parse() throws IOException, ConnectionClosedException, HttpRequestFormatException;
}
