package com.codemerx.http;

public class HttpResponse extends HttpMessage {
    private final HttpStatusLine statusLine;

    public HttpResponse(HttpStatusLine statusLine) {
        this.statusLine = statusLine;
    }

    public HttpStatusLine getStatusLine() {
        return this.statusLine;
    }
}
