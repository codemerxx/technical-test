package com.codemerx.http;

public class HttpStatusLine {
    private final Protocol protocol;
    private final HttpStatus status;

    public HttpStatusLine(Protocol protocol, HttpStatus status) {
        this.protocol = protocol;
        this.status = status;
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public HttpStatus getStatus() {
        return this.status;
    }
}
