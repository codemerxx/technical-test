package com.codemerx.http;

import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

public class LoggingHttpHandler implements Handler<HttpRequest, HttpResponse> {
    private final Handler<HttpRequest, HttpResponse> httpHandler;

    public LoggingHttpHandler(Handler<HttpRequest, HttpResponse> httpHandler) {
        this.httpHandler = httpHandler;
    }

    @Override
    public HttpResponse handle(final HttpRequest httpRequest, final ApplicationContext applicationContext) throws Throwable {
        final Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
        final HttpRequestLine requestLine = httpRequest.getRequestLine();
        logger.info(String.format("%s %s", requestLine.getMethod(), requestLine.getUri()));

        final HttpResponse httpResponse = this.httpHandler.handle(httpRequest, applicationContext);

        final HttpStatus responseStatus = httpResponse.getStatusLine().getStatus();
        logger.info(String.format("%s %s", responseStatus.getCode(), responseStatus.getReason()));

        return httpResponse;
    }
}
