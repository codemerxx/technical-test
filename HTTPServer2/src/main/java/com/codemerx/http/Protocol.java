package com.codemerx.http;

public class Protocol {
    private final String protocolName;
    private final int majorVersion;
    private final int minorVersion;

    public Protocol(String protocolName, int majorVersion, int minorVersion) {
        this.protocolName = protocolName;
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    public String getProtocolName() {
        return this.protocolName;
    }

    public int getMajorVersion() {
        return this.majorVersion;
    }

    public int getMinorVersion() {
        return this.minorVersion;
    }
}
