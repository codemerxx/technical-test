package com.codemerx.http;

import java.io.IOException;

public interface ResponseWriter {
    void write(HttpResponse response) throws IOException, ConnectionClosedException;
}
