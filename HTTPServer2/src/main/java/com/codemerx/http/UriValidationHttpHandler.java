package com.codemerx.http;

import com.codemerx.factories.HttpResponseFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;
import com.codemerx.web.Handler;

import java.io.File;
import java.nio.file.Path;

public class UriValidationHttpHandler implements Handler<HttpRequest, HttpResponse> {
    private final HttpResponseFactory httpResponseFactory;
    private final Handler<HttpRequest, HttpResponse> httpHandler;

    public UriValidationHttpHandler(HttpResponseFactory httpResponseFactory, Handler<HttpRequest, HttpResponse> httpHandler) {
        this.httpResponseFactory = httpResponseFactory;
        this.httpHandler = httpHandler;
    }

    @Override
    public HttpResponse handle(final HttpRequest httpRequest, final ApplicationContext applicationContext) throws Throwable {
        final Logger logger = applicationContext.getLoggerFactory().createLogger(this.getClass());
        final Path rootPath = Path.of(applicationContext.getConfig().getRootFolder());
        final Path resourceFilePath = Path.of(rootPath.toString(), httpRequest.getRequestLine().getUri()).normalize();

        if (!this.isSubDirectory(rootPath, resourceFilePath)) {
            return this.createResponseWithErrorCode(HttpStatus.FORBIDDEN, logger);
        }

        final File resourceFile = resourceFilePath.toFile();
        if (!resourceFile.exists()) {
            return this.createResponseWithErrorCode(HttpStatus.NOT_FOUND, logger);
        }

        if (!resourceFile.canRead()) {
            return this.createResponseWithErrorCode(HttpStatus.FORBIDDEN, logger);
        }

        if (resourceFile.isDirectory() && !Path.of(resourceFilePath.toString(), applicationContext.getConfig().getIndexFilePath()).toFile().exists()) {
            return this.createResponseWithErrorCode(HttpStatus.FORBIDDEN, logger);
        }

        return this.httpHandler.handle(httpRequest, applicationContext);
    }

    private boolean isSubDirectory(final Path base, final Path child) {
        Path parentPath = child;
        final Path baseAbsolutePath = base.toAbsolutePath();

        while (parentPath != null) {
            if (baseAbsolutePath.equals(parentPath.toAbsolutePath())) {
                return true;
            }

            parentPath = parentPath.getParent();
        }

        return false;
    }

    private HttpResponse createResponseWithErrorCode(final HttpStatus status, final Logger logger) {
        final HttpResponse httpResponse = this.httpResponseFactory.createResponse(status);
        logger.info("Invalid request! Returning response with code: " + status.getCode());

        return httpResponse;
    }
}
