package com.codemerx.utils;

import com.codemerx.factories.LoggerFactory;
import com.codemerx.web.Config;

public class ApplicationContext {
    private final Config config;
    private final LoggerFactory loggerFactory;

    public ApplicationContext(Config config, LoggerFactory loggerFactory) {
        this.config = config;
        this.loggerFactory = loggerFactory;
    }

    public Config getConfig() {
        return this.config;
    }

    public LoggerFactory getLoggerFactory() {
        return this.loggerFactory;
    }
}
