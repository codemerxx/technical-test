package com.codemerx.utils;

public class ArgumentsParser {
    private static final int DEFAULT_PORT_NUMBER = 3456;
    private static final int DEFAULT_MAX_POOL_SIZE = 10;
    private static final int DEFAULT_MAX_KEEP_ALIVE_CONNECTIONS_POOL_SIZE = 64;
    private static final int DEFAULT_KEEP_ALIVE_CONNECTION_TIMEOUT = 10;
    private static final boolean DEFAULT_VERBOSE = false;

    private static final String ARGUMENT_PREFIX = "--";
    private static final String SEPARATOR = "=";

    private static final String PORT_ARGUMENT_NAME = "port";
    private static final String PORT_ARGUMENT = ARGUMENT_PREFIX + PORT_ARGUMENT_NAME;
    private static final String MAX_POOL_SIZE_ARGUMENT_NAME = "maxPoolSize";
    private static final String MAX_POOL_SIZE_ARGUMENT = ARGUMENT_PREFIX + MAX_POOL_SIZE_ARGUMENT_NAME;
    private static final String MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT_NAME = "maxKeepAlivePoolSize";
    private static final String MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT = ARGUMENT_PREFIX + MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT_NAME;
    private static final String KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT_NAME = "keepAliveConnectionTimeout";
    private static final String KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT = ARGUMENT_PREFIX + KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT_NAME;
    private static final String VERBOSE_ARGUMENT = ARGUMENT_PREFIX + "verbose";

    private static final int START_PORT = 1024;
    private static final int END_PORT = 65535;

    private static final String FAILED_TO_PARSE_MESSAGE_FORMAT = "Failed to parse the %s argument. Falling back to default.";

    public Arguments parse(final String[] args) {
        final Arguments result = this.createDefaultArguments();

        for (final String arg : args) {
            if (VERBOSE_ARGUMENT.equalsIgnoreCase(arg)) {
                result.verbose = true;
            } else {
                final String[] argumentComponents = arg.split(SEPARATOR);
                if (argumentComponents.length != 2) {
                    System.out.println(String.format("Unexpected parameter %s. Falling back to default configuration.", arg));
                    return this.createDefaultArguments();
                }

                String name = argumentComponents[0];
                String value = argumentComponents[1];

                if (PORT_ARGUMENT.equalsIgnoreCase(name)) {
                    try {
                        result.port = Integer.parseInt(value);
                    } catch (NumberFormatException ignore) {
                        System.out.println(String.format(FAILED_TO_PARSE_MESSAGE_FORMAT, PORT_ARGUMENT_NAME));
                        return this.createDefaultArguments();
                    }
                } else if (MAX_POOL_SIZE_ARGUMENT.equalsIgnoreCase(name)) {
                    try {
                        result.maxPoolSize = Integer.parseInt(value);
                    } catch (NumberFormatException ignore) {
                        System.out.println(String.format(FAILED_TO_PARSE_MESSAGE_FORMAT, MAX_POOL_SIZE_ARGUMENT_NAME));
                        return this.createDefaultArguments();
                    }
                } else if (MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT.equalsIgnoreCase(name)) {
                    try {
                        result.maxKeepAliveConnectionsPoolSize = Integer.parseInt(value);
                    } catch (NumberFormatException ignore) {
                        System.out.println(String.format(FAILED_TO_PARSE_MESSAGE_FORMAT, MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT_NAME));
                        return this.createDefaultArguments();
                    }
                } else if (KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT.equalsIgnoreCase(name)) {
                    try {
                        result.keepAliveConnectionTimeout = Integer.parseInt(value);
                    } catch (NumberFormatException ignore) {
                        System.out.println(String.format(FAILED_TO_PARSE_MESSAGE_FORMAT, KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT_NAME));
                        return this.createDefaultArguments();
                    }
                } else {
                    System.out.println(String.format("Ignoring unrecognized argument - %s.", arg));
                }
            }
        }

        ValidationResult validationResult = this.validate(result);
        if (validationResult.isSuccessful) {
            return result;
        } else {
            System.out.println(String.format("Argument validation failed - %s. Falling back to default.", validationResult.errorMessage));
            return this.createDefaultArguments();
        }
    }

    private ValidationResult validate(final Arguments result) {
        if (result.port < START_PORT || END_PORT < result.port ) {
            return ValidationResult.failure(String.format("Invalid value for %s. It should be between 1024 and 65535, inclusive", PORT_ARGUMENT_NAME));
        }

        if (result.maxPoolSize <= 0) {
            return ValidationResult.failure(String.format("Invalid value for %s. It should be positive integer", MAX_POOL_SIZE_ARGUMENT_NAME));
        }

        if (result.maxKeepAliveConnectionsPoolSize <= 0) {
            return ValidationResult.failure(String.format("Invalid value for %s. It should be positive integer", MAX_KEEP_ALIVE_POOL_SIZE_ARGUMENT_NAME));
        }

        if (result.keepAliveConnectionTimeout <= 0) {
            return ValidationResult.failure(String.format("Invalid value for %s. It should be positive integer", KEEP_ALIVE_CONNECTION_TIMEOUT_ARGUMENT_NAME));
        }

        return ValidationResult.success();
    }

    private Arguments createDefaultArguments() {
        return new Arguments(DEFAULT_PORT_NUMBER, DEFAULT_MAX_POOL_SIZE, DEFAULT_MAX_KEEP_ALIVE_CONNECTIONS_POOL_SIZE, DEFAULT_KEEP_ALIVE_CONNECTION_TIMEOUT, DEFAULT_VERBOSE);
    }

    public static class Arguments {
        private int port;
        private int maxPoolSize;
        private int maxKeepAliveConnectionsPoolSize;
        private int keepAliveConnectionTimeout;
        private boolean verbose;

        private Arguments(final int port, final int maxPoolSize, final int maxKeepAliveConnectionsPoolSize, final int keepAliveConnectionTimeout, final boolean verbose) {
            this.port = port;
            this.maxPoolSize = maxPoolSize;
            this.maxKeepAliveConnectionsPoolSize = maxKeepAliveConnectionsPoolSize;
            this.keepAliveConnectionTimeout = keepAliveConnectionTimeout;
            this.verbose = verbose;
        }

        public int getPort() {
            return this.port;
        }

        public int getMaxPoolSize() {
            return this.maxPoolSize;
        }

        public int getMaxKeepAliveConnectionsPoolSize() {
            return this.maxKeepAliveConnectionsPoolSize;
        }

        public int getKeepAliveConnectionTimeout() {
            return this.keepAliveConnectionTimeout;
        }

        public boolean getVerbose() {
            return verbose;
        }
    }

    private static class ValidationResult {
        private final boolean isSuccessful;
        private final String errorMessage;

        private ValidationResult(final boolean isSuccessful, final String errorMessage) {
            this.isSuccessful = isSuccessful;
            this.errorMessage = errorMessage;
        }

        private static ValidationResult success() {
            return new ValidationResult(true, null);
        }

        private static ValidationResult failure(final String errorMessage) {
            return new ValidationResult(false, errorMessage);
        }
    }
}
