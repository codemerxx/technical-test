package com.codemerx.utils;

import org.apache.log4j.Level;

public class Log4JLogger implements Logger {
    private final org.apache.log4j.Logger logger;
    private final boolean isVerbose;

    public Log4JLogger(final Class clazz, boolean isVerbose) {
        this.logger = org.apache.log4j.Logger.getLogger(clazz);
        this.isVerbose = isVerbose;
    }

    @Override
    public void error(final String message) {
        this.logger.error(message);
    }

    @Override
    public void info(final String message) {
        this.logger.info(message);
    }

    @Override
    public void warning(final String message) {
        this.logger.warn(message);
    }

    @Override
    public void verbose(final String message) {
        if (this.isVerbose) {
            this.logger.info(message);
        }
    }
}
