package com.codemerx.utils;

public interface Logger {
    void error(String message);

    void info(String message);

    void warning(String message);

    void verbose(final String message);
}
