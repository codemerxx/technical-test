package com.codemerx.web;

public class Config {
    private static final String INDEX_HTML = "index.html";
    private static final int MAX_RESPONSE_GENERATION_POOL_SIZE = 16;

    private final String rootFolder;
    private final int maxPoolSize;
    private final String indexFilePath;
    private final int maxSendKeepAliveResponsePoolSize;
    private final int maxKeepAliveConnectionsPoolSize;
    private final int keepAliveTimeout;

    public Config(String rootFolder, int maxPoolSize, int maxKeepAliveConnectionsPoolSize, int keepAliveTimeout) {
        this(rootFolder, maxPoolSize, maxKeepAliveConnectionsPoolSize, keepAliveTimeout, MAX_RESPONSE_GENERATION_POOL_SIZE, INDEX_HTML);
    }

    public Config(String rootFolder, int maxPoolSize, int maxKeepAliveConnectionsPoolSize, int keepAliveTimeout, int maxSendKeepAliveResponsePoolSize, String indexFilePath) {
        this.rootFolder = rootFolder;
        this.maxPoolSize = maxPoolSize;
        this.maxKeepAliveConnectionsPoolSize = maxKeepAliveConnectionsPoolSize;
        this.keepAliveTimeout = keepAliveTimeout;
        this.maxSendKeepAliveResponsePoolSize = maxSendKeepAliveResponsePoolSize;
        this.indexFilePath = indexFilePath;
    }

    public String getRootFolder() {
        return this.rootFolder;
    }

    public int getMaxPoolSize() {
        return this.maxPoolSize;
    }

    public int getMaxKeepAliveConnectionsPoolSize() {
        return this.maxKeepAliveConnectionsPoolSize;
    }

    public int getKeepAliveTimeout() {
        return this.keepAliveTimeout;
    }

    public int getMaxSendKeepAliveResponsesPoolSize() {
        return this.maxSendKeepAliveResponsePoolSize;
    }

    public String getIndexFilePath() {
        return this.indexFilePath;
    }
}
