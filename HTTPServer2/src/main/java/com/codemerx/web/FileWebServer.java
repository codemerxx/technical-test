package com.codemerx.web;

import com.codemerx.factories.DefaultHttpResponseFactory;
import com.codemerx.http.*;
import com.codemerx.utils.ApplicationContext;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileWebServer {
    private final ApplicationContext applicationContext;

    public FileWebServer(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public Thread listen(final int port) throws IOException {
        final ServerSocket serverSocket = new ServerSocket(port);

        int maxPoolSize = this.applicationContext.getConfig().getMaxPoolSize();
        final ExecutorService executor = Executors.newFixedThreadPool(maxPoolSize);

        final DefaultHttpResponseFactory factory = new DefaultHttpResponseFactory();
        final FileManipulationHttpHandler fileManipulationHttpHandler = new FileManipulationHttpHandler(factory);
        final UriValidationHttpHandler uriValidationHttpHandler = new UriValidationHttpHandler(factory, fileManipulationHttpHandler);
        final CheckHttpMethodHandler checkHttpMethodHandler = new CheckHttpMethodHandler(factory, uriValidationHttpHandler);

        final ErrorHttpHandler errorHttpHandler = new ErrorHttpHandler(factory, checkHttpMethodHandler);

        final LoggingHttpHandler loggingHttpHandler = new LoggingHttpHandler(errorHttpHandler);

        BasicHttpHandler basicHttpHandler = new BasicHttpHandler(factory, loggingHttpHandler);

        final WebServer webServer = new WebServer(this.applicationContext, serverSocket, executor, basicHttpHandler);

        final Thread thread = new Thread(webServer);
        thread.start();

        return thread;
    }
}
