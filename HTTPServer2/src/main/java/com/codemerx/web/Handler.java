package com.codemerx.web;

import com.codemerx.utils.ApplicationContext;

public interface Handler<TInput, TOutput> {
    TOutput handle(TInput input, ApplicationContext applicationContext) throws Throwable;
}
