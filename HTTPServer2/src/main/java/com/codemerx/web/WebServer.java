package com.codemerx.web;

import com.codemerx.utils.ApplicationContext;
import com.codemerx.utils.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

public class WebServer implements Runnable {
    private final ApplicationContext applicationContext;
    private final ServerSocket serverSocket;
    private final ExecutorService executorService;
    private final Handler<Socket, Void> handler;

    public WebServer(
            ApplicationContext applicationContext,
            ServerSocket serverSocket,
            ExecutorService executorService,
            Handler<Socket, Void> handler) {

        this.serverSocket = serverSocket;
        this.executorService = executorService;
        this.handler = handler;
        this.applicationContext = applicationContext;
    }

    @Override
    public void run() {
        final Logger logger = this.applicationContext.getLoggerFactory().createLogger(this.getClass());
        while (!Thread.interrupted()) {
            try {
                final Socket socket = this.serverSocket.accept();

                this.executorService.execute(() -> {
                    try {
                        this.handler.handle(socket, this.applicationContext);
                    } catch (Throwable e) {
                        logger.error("Handled exception while serving request: " + e.getMessage());
                    } finally {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            logger.error("Handled exception while serving request: " + e.getMessage());
                        }
                    }
                });
            } catch (IOException e) {
                logger.error("Handled exception while accepting socket: " + e.getMessage());
            }
        }
    }
}
