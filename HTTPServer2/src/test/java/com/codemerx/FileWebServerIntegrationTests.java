package com.codemerx;

import com.codemerx.factories.Log4JLoggerFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.web.Config;
import com.codemerx.web.FileWebServer;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.util.Optional;

public class FileWebServerIntegrationTests {
    private static Thread serverThread;
    private static final int PORT_NUMBER = 1111;
    private static final int OK = 200;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int METHOD_NOT_IMPLEMENTED = 501;
    private static final String Expected = "<html>\r\n" +
            "<body>\r\n" +
            "<span>Hello World!</span>\r\n" +
            "</body>\r\n" +
            "</html>";

    @BeforeClass
    public static void setup() {
        String userDir = System.getProperty("user.dir");
        Path path = Path.of(userDir);
        String rootFolderPath = path.toString();
        int cores = Runtime.getRuntime().availableProcessors();
        int keepAliveConnectionsPoolSize = 16;
        int keepAliveConnectionTimeout = 10;
        Config config = new Config(rootFolderPath, cores, keepAliveConnectionsPoolSize, keepAliveConnectionTimeout);

        FileWebServer fileWebServer = new FileWebServer(new ApplicationContext(config, new Log4JLoggerFactory(true)));

        try {
            serverThread = fileWebServer.listen(PORT_NUMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void cleanUp() {
        serverThread.interrupt();
    }

    @Test
    public void whenGetRequest_ShouldReturnCorrectResponseBody() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(Expected, response.body());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequest_ShouldReturnCorrectResponseCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(OK, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequest_ShouldReturnCorrectResponseContentLength() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());

            Optional header = response.headers().firstValue("Content-Length");
            Assert.assertTrue(header.isPresent());
            Assert.assertEquals("59", header.get());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenPostRequest_ShouldReturnReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .POST(HttpRequest.BodyPublishers.noBody())
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(METHOD_NOT_IMPLEMENTED, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenPutRequest_ShouldReturnReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .PUT(HttpRequest.BodyPublishers.noBody())
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(METHOD_NOT_IMPLEMENTED, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenDeleteRequest_ShouldReturnReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/sample.html"))
                .DELETE()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(METHOD_NOT_IMPLEMENTED, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequestFileNotFound_ShouldReturnReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources/not-found.html"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(NOT_FOUND, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequestFileInParentFolder_ShouldReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/../settings.gradle"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(FORBIDDEN, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequestRootFolder_ShouldReturnCorrectErrorCode() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(FORBIDDEN, response.statusCode());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void whenGetRequestToFolder_ShouldReturnResponseWithIndexHtml() {
        // Arrange
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:1111/src/test/resources"))
                .GET()
                .build();

        // Act && Assert
        try {
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
            Assert.assertEquals(Expected, response.body());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
