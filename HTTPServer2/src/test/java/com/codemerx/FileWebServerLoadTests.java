package com.codemerx;

import com.codemerx.factories.Log4JLoggerFactory;
import com.codemerx.utils.ApplicationContext;
import com.codemerx.web.Config;
import com.codemerx.web.FileWebServer;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.loadtest4j.LoadTester;
import org.loadtest4j.Request;
import org.loadtest4j.Result;
import org.loadtest4j.drivers.jmeter.JMeterBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileWebServerLoadTests {
    private static Thread serverThread;
    private static final int PORT_NUMBER = 5555;

    @BeforeClass
    public static void setup() {
        String userDir = System.getProperty("user.dir");
        Path path = Paths.get(userDir);
        String rootFolderPath = path.toString();
        int cores = Runtime.getRuntime().availableProcessors();
        int keepAliveConnectionsPoolSize = 16;
        int keepAliveConnectionTimeout = 10;
        Config config = new Config(rootFolderPath, cores, keepAliveConnectionsPoolSize, keepAliveConnectionTimeout);

        FileWebServer fileWebServer = new FileWebServer(new ApplicationContext(config, new Log4JLoggerFactory(true)));

        try {
            serverThread = fileWebServer.listen(PORT_NUMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void cleanUp() {
        serverThread.interrupt();
    }

    @Test
    public void whenManyGetRequests_ShouldHandleOverload() {
        // Arrange
        LoadTester loadTester = JMeterBuilder.withUrl("http", "localhost", PORT_NUMBER)
                .withNumThreads(1000)
                .withRampUp(5)
                .build();

        // Act
        List<Request> requests = List.of(Request.get("/src/test/resources/sample.html"));
        Result result = loadTester.run(requests);

        // Assert
        Assert.assertEquals(100, result.getPercentOk(), 0.1);
    }
}
