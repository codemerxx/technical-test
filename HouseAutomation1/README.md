# House Automation App

The House Automation app's purpose is to help manage the automation components/sensors in a smart home environment.

## Getting Started

The application can be started in two modes - with or without data persistence. Data persistence is achieved using a Node.js server to update the data source. The other mode (the one without data persistence) reads data from a static file.

### Prerequisites
You must have [Node.js](https://nodejs.org/en/) version 10.14.x and [npm](https://www.npmjs.com/) installed.

You must install the application node modules:
```
npm run installPackages
```

### Starting the application with the sample web servers included
To start the application without data persistence capabilities run:
```
npm run start:noDataPersist
```
in the project root directory.

To start the application with data persistence capabilities run:
```
npm start
```
in the project root directory.

### Building the application to run with a custom web server

To build the application:
```
npm run build:app
```

The build output is located in ```app/dist``` folder. The custom web server should serve this folder.

## Functionality
The app presents the following functionality:

 * Viewing the existing components either grouped by their type (e.g. light switch, etc.) or by location (e.g. Room, etc.)
 * Modify the components state

## Extensibility

A dependency injection framework (Inversify) is used to manage the lifetime of all components. To author a new component you must create a class that implements the IAutomationComponent interface and place that class at a predefined location where the DI framework searches for components. The DI framework will then take care of component discovery and instantiation.

Last but not least, you must add any data the component might need to the ```.json``` files used by the app for data storage.

### Setup

To add a new component you must:

 1. Create a new class that implements the [IAutomationComponent](#IAutomationComponent) interface
 2. The class must be ( located in the ```app/src/components``` directory )
 3. Add a new data record to the ```data-source.json``` file ( located in ```dataSource``` directory )
 4. Optionally you can add custom stylesheet for a component instance or component type that will apply custom css styles to that component. You can do so by adding a CSS file in ```app/src/components/styles``` folder and setting the component ```styleSheetName``` field to the name of the created file

Optional - adding a new grouping criteria:
 
 1. Add a new group type in the ```data-source.json``` file ( located in the ```dataSource``` directory )
 2. Modify the ```getGroupName``` function ( located in ```app/src/ControlPanel.ts``` ) to return a new group based on a component field or based on some more advanced logic. ( NOTE: If the newly implemented group has to depend on a component field, the field should be added to the ```ComponentBase.ts```' ( located in ```app/src/components``` directory ) fields, to the ```initialize``` function in the same file and to the invocation of the ```initalize``` function ( located in ```app/src/ControlPanel.ts``` )

### API Reference

#### IAutomationComponent

Fields:
  
- ```type``` -The component type (e.g. Light, Curtains etc.). 
- ```name``` - The unique component identifier (e.g. kitchen-light-1)
- ```displayName``` - The component display name (e.g. Main Kitchen Light)
- ```room``` - The room that the component is assigned to (e.g. Kitchen)
- ```value``` - The value that represents the current component state (e.g. 'value: true' means that the Light component instance is switched on)

Optional fields:
 
- ```stylesheetName``` - The component stylesheet name. If set, the component will try loading the stylesheet styles and applying them.

Methods:

 - ```initialize(name, displayName, room, value)``` - The framework calls this method to pass initialization data to the component (the values currently come from the ```data-source.json``` file). It will be called before calling the ```render``` method.
 - ```render()``` - The framework calls this method when it needs to render the component. The method should return the parent ```HTMLElement``` of the component.

## Details
* The application dynamically loads the UI. JQuery is used to construct the UI components.
* The Inversify dependency injection library is used.
* [Materialize](https://materializecss.com/) is the styles library of choice.
* The source code is all custom code written by me except for the external libraries.
* Tested on Chrome, Firefox and Microsoft Edge

## Pre-compiled Application

Best effort has been made to test the build process on as many platforms as possible. As a backup scenario a pre-compiled application is also provided in the ```./packaged-app``` folder. Any http server should be able to serve the pre-compiled version. An easy way to try it would be:

```
npm i -g http-server
http-server ./packaged-app -p 8081 -a localhost -o http://localhost:8081/index.html
```

in the root folder of this repo.
