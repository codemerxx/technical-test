(require => {
    const gulp = require('gulp');
    const webpack = require('webpack');
    const args = require('yargs').argv;
    const webpackConfig = require('./webpack.config')({
        serverType: args.serverType
    });

    const copyResourceTasks = [copyEntryHtmlFile, copyExternalLibs, copyAppStyles, copyComponentStylesheets];

    if (args.serverType === 'local') {
        copyResourceTasks.push(copyDataSource);
    }

    gulp.task('copyResources', gulp.parallel(...copyResourceTasks));
    gulp.task('build', gulp.series(runWebpack, 'copyResources'));

    function runWebpack() {
        return new Promise((resolve, reject) => {
            webpack(webpackConfig, (err, stats) => {
                if (err) {
                    return reject(err);
                }

                if (stats.hasErrors()) {
                    return reject(new Error(stats.compilation.errors.map(x => x.message ? x.message : x).join('\n')));
                }

                resolve();
            });
        });
    }

    function copyEntryHtmlFile() {
        return gulp.src('./src/index.html')
            .pipe(gulp.dest('./dist'));
    }

    function copyDataSource() {
        return gulp.src('../dataSource/data-source.json')
            .pipe(gulp.dest('./dist/dataSource'));
    }

    function copyExternalLibs() {
        return gulp.src('./libs/**/*')
            .pipe(gulp.dest('./dist/libs'));
    }

    function copyAppStyles() {
        return gulp.src('./styles/**/*')
            .pipe(gulp.dest('./dist/styles'));
    }

    function copyComponentStylesheets() {
        return gulp.src('./src/components/styles/**/*')
            .pipe(gulp.dest('./dist/componentStyles'));
    }

})(require);
