import { injectable, inject, named } from 'inversify';

import container from './container.config';
import { Path, Contract, Service, GroupType } from './common/enums';
import { IAutomationComponent, IDataProvider } from './common/interfaces';
import { RenderHelper, ComponentValidator } from './utils';

@injectable()
export default class ControlPanel {
    private automationComponents: IAutomationComponent[];
    private groupTypes: GroupType[];
    private selectedGroupType: GroupType;
    private groupTypeToSelectedGroupIndexMap: Map<GroupType, number>;

    constructor(
        @inject(Contract.DataProvider) @named(Service.ComponentService) private componentsProvider: IDataProvider,
        @inject(Contract.DataProvider) @named(Service.GroupService) private groupTypesProvider: IDataProvider
        ) {
            this.groupTypeToSelectedGroupIndexMap = new Map<GroupType, number>();
        }

    public initialize() : void {
        Promise.all([ this.loadGroupTypes(), this.loadComponentInstances() ])
            .then(result => {
                const [groupTypes, componentInstances] = result;

                this.automationComponents = componentInstances;
                this.groupTypes = groupTypes.map(groupType => groupType as GroupType);

                this.groupTypes.forEach(groupType => {
                    if (!this.groupTypeToSelectedGroupIndexMap.has(groupType)) {
                        this.groupTypeToSelectedGroupIndexMap.set(groupType, 0);
                    }
                });

                this.renderControlPanel(this.groupTypes, this.automationComponents);
            });
    }

    private loadGroupTypes() : Promise<string[]> {
        return this.groupTypesProvider.getAll();
    }

    private loadComponentInstances() : Promise<IAutomationComponent[]> {
        return this.componentsProvider.getAll()
            .then((componentsData: any) => {
                ComponentValidator.verifyComponentNamesAreUnique(componentsData.map((componentData: any) => componentData.name));
                
                const automationComponents: IAutomationComponent[] = [];

                componentsData.forEach((componentData: any) => {
                    const componentInstance = container.getNamed<IAutomationComponent>(Contract.AutomationComponent, componentData.type);

                    componentInstance.initialize(componentData.name, componentData.displayName, componentData.room, componentData.value);

                    if (componentInstance.stylesheetName) {
                        appendComponentStylesheet([ Path.ComponentStyleSheets, componentInstance.stylesheetName ].join('/'));
                    }

                    automationComponents.push(componentInstance);
                });

                return automationComponents;
            });
    }

    private renderControlPanel(groupTypes: GroupType[], automationComponents: IAutomationComponent[]) : void {
        const controlPanelElement = document.getElementById('controlPanel');

        if (automationComponents.length > 0) {
            const groupByButtonElements = this.renderGroupByButtons(groupTypes, automationComponents);

            this.selectedGroupType = groupTypes[0];
            const componentGroups = this.groupComponents(automationComponents, this.selectedGroupType);

            this.renderComponentGroups(componentGroups, this.selectedGroupType);
            controlPanelElement.prepend(groupByButtonElements);
        } else {
            controlPanelElement.textContent = 'No automation components registered!';
        }
    }

    private renderGroupByButtons(groupTypes: GroupType[], automationComponents: IAutomationComponent[]) : HTMLElement {
        const containerElement = document.createElement('div');

        groupTypes.forEach((groupType: GroupType, groupIndex: number) => {
            const buttonElement = RenderHelper.renderButton({
                text: groupType,
                attributes: [
                    { key: 'id', value: groupType }
                ],
                events: [
                    {
                        type: 'click',
                        callback: (buttonElement: HTMLElement) => {
                            RenderHelper.setActiveButtonGroupItem(containerElement, buttonElement);

                            this.selectedGroupType = groupType;
                            const componentGroups = this.groupComponents(automationComponents, this.selectedGroupType);

                            this.renderComponentGroups(componentGroups, this.selectedGroupType);
                        }
                    }
                ]
            });

            if (groupIndex === 0) {
                RenderHelper.setActiveButtonGroupItem(containerElement, buttonElement);
            }

            containerElement.appendChild(buttonElement);
        });

        return containerElement;
    }

    private clearPreviousChildElements(containerElement: HTMLElement) : void {
        while (containerElement.firstChild) {
            containerElement.firstChild.remove();
        }
    }

    private renderComponentGroups(componentGroups: Map<string, IAutomationComponent[]>, groupType: GroupType) : void {
        const componentGroupsContainer = document.getElementById('component-groups-container');
        
        this.clearPreviousChildElements(componentGroupsContainer);

        const selectedTabIndex = this.groupTypeToSelectedGroupIndexMap.get(groupType);

        const tabElements = RenderHelper.renderTabs(Array.from(componentGroups.keys()), selectedTabIndex, (tabIndex: number) => {
            this.groupTypeToSelectedGroupIndexMap.set(groupType, tabIndex);
        });

        componentGroupsContainer.appendChild(tabElements);

        const groupElements: HTMLElement[] = [];

        for(let groupName of componentGroups.keys()) {
            const groupComponents = componentGroups.get(groupName);
            const renderedComponents: HTMLElement[] = [];

            groupComponents.forEach(component => {
                renderedComponents.push(component.render());
            });

            const rowElement = RenderHelper.renderRow(renderedComponents);
            const tabContentContainer = RenderHelper.renderColumn(rowElement, {
                smallScreenWidth: 12,
                mediumScreenWidth: 12,
                attributes: [
                    {key: 'id', value: RenderHelper.normalizeString(groupName) }
                ]
            });
    
            groupElements.push(tabContentContainer);
        }

        groupElements.forEach(element => {
            componentGroupsContainer.appendChild(element);
        });
        M.Tabs.init(tabElements);
    }

    private groupComponents(automationComponents: IAutomationComponent[], groupType: GroupType) : Map<string, IAutomationComponent[]> {
        const componentGroups = new Map<string, IAutomationComponent[]>();

        automationComponents.forEach(component => {
            const groupName = getGroupName(component, groupType);

            if (!componentGroups.has(groupName)) {
                componentGroups.set(groupName, [ component ]);
            } else {
                componentGroups.get(groupName).push(component);
            }
        });

        return componentGroups;
    }
}

function getGroupName(component: IAutomationComponent, groupType: GroupType) : string {
    switch (groupType) {
        case GroupType.Type:
            return component.type;
        case GroupType.Room:
            return component.room;
        default:
            return component.type;
    }
}

function appendComponentStylesheet(stylesheetUrl: string) : void {
    const linkElement = document.createElement('link');  
    linkElement.rel = 'stylesheet';  
    linkElement.type = 'text/css'; 
    linkElement.href = stylesheetUrl;  

    document.getElementsByTagName('HEAD')[0].appendChild(linkElement);  
}
