export enum Contract {
    AutomationComponent = 'IAutomationComponent',
    RequestService = 'IRequestService',
    DataProvider = 'IDataProvider'
};