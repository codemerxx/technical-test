export enum Path {
    ComponentStyleSheets = '/componentStyles',
    ComponentsUrl = '/components',
    GroupTypesUrl = '/groupTypes'
};
