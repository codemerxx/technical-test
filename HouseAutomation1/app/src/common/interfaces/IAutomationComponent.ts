export default interface IAutomationComponent {
    type: string;
    name: string;
    displayName: string;
    room: string;
    value: any;
    stylesheetName?: string;

    initialize(name: string, displayName: string, room: string, value: (boolean | number)) : void;
    render() : HTMLElement;
};
