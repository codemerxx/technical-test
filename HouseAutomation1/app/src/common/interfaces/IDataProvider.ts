export default interface IDataProvider {
    getAll() : any,
    get(componentName: string) : any,
    update(componentName: string, payload: any) : Promise<void>;
}
