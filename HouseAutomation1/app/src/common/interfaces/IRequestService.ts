import { RequestOptions, Response } from '../types';

export default interface IRequestService {
    get(url: string, options?: RequestOptions) : Promise<Response>;
    post(url: string, options: RequestOptions) : Promise<Response>;
};
