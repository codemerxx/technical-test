import IAutomationComponent from './IAutomationComponent';
import IRequestService from './IRequestService';
import IDataProvider from './IDataProvider';

export {
    IAutomationComponent,
    IRequestService,
    IDataProvider
};
