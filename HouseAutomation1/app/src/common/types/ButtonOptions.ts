import { ElementAttribute } from './ElementAttribute';
import { ElementEvent } from './ElementEvent';

export type ButtonOptions = {
    text: string,
    attributes?: ElementAttribute[],
    events?: ElementEvent[]
}