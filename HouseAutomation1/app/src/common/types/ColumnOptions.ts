import { ElementAttribute } from './ElementAttribute';

export type ColumnOptions = {
    attributes?: ElementAttribute[],
    smallScreenWidth?: number,
    mediumScreenWidth?: number,
    largeScreenWidth?: number
}
