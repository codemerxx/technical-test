export type ElementEvent = {
    type: string,
    callback(targetElement: HTMLElement): void;
};
