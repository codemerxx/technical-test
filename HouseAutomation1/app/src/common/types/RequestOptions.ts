export type RequestOptions = {
    headers?: HeadersInit
    method?: string;
    body?: any,
    json?: boolean
};
