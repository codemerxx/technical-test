export type Response = {
    readonly status: number,
    body?: string
};
