import { injectable } from 'inversify';

@injectable()
export default abstract class ComponentBase {
    public name: string;
    public displayName: string;
    public room: string;
    public value: (boolean | number);
    public stylesheetName: string;

    public initialize(name: string, displayName: string, room: string, value: (boolean | number)) : void {
        this.name = name;
        this.displayName = displayName;
        this.room = room;
        this.value = value;
    }
};