import jquery from 'jquery';
import { injectable, inject, named } from 'inversify';

import { Contract, Service, Component } from '../common/enums';
import { RenderHelper, ComponentValidator } from '../utils';
import { IAutomationComponent, IDataProvider } from '../common/interfaces';
import ComponentBase from './ComponentBase';
import { ColumnOptions } from '../common/types';

@injectable()
export default class CurtainsComponent extends ComponentBase implements IAutomationComponent {
    public type: string;
    private columnContainerOptions: ColumnOptions

    constructor(@inject(Contract.DataProvider) @named(Service.ComponentService) private dataProvider: IDataProvider) { 
        super();
        this.type = Component.Curtains;
        this.columnContainerOptions = {
            mediumScreenWidth: 6,
            smallScreenWidth: 12,
            largeScreenWidth: 3
        };
    }

    public render() : HTMLElement {
        const componentTitle = this.displayName || this.name;

        ComponentValidator.verifyComponentNamesAreSet(Component.Curtains, this.name, this.displayName);

        const switchContainer = RenderHelper.renderSwitch('Closed', 'Opened', this.value as boolean);
        const componentContainer = RenderHelper.renderComponentContainer(componentTitle, switchContainer, this.columnContainerOptions);

        this.changeCardBackground(jquery(componentContainer).find('.card'), this.value as boolean);

        jquery(switchContainer).find('#switchInput').on('click', ev => {
            const newValue = jquery(ev.target).prop('checked');

            if (newValue !== this.value) {
                this.dataProvider.update(this.name, {
                    value: newValue
                })
                .then(() => {
                    this.value = newValue;

                    this.changeCardBackground(jquery(componentContainer).find('.card'), newValue);
                });
            }
        });

        return componentContainer;
    }

    private changeCardBackground(cardContainer: JQuery<HTMLElement>, componentState: boolean) : void {
        if (!componentState) {
            cardContainer.addClass('grey lighten-2');
        } else {
            cardContainer.removeClass('grey lighten-2');
        }
    }
}
