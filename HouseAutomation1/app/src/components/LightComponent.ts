import jquery from 'jquery';
import { injectable, inject, named } from 'inversify';

import { Contract, Component, Service } from '../common/enums';
import { RenderHelper, ComponentValidator } from '../utils';
import { IAutomationComponent, IDataProvider } from '../common/interfaces';
import ComponentBase from './ComponentBase';
import { ColumnOptions } from '../common/types';

@injectable()
export default class LightComponent extends ComponentBase implements IAutomationComponent {
    public type: string;
    private columnContainerOptions: ColumnOptions;
    
    constructor(@inject(Contract.DataProvider) @named(Service.ComponentService) private dataProvider: IDataProvider) { 
        super();
        this.type = Component.Light;
        this.stylesheetName = 'light-component.css';
        this.columnContainerOptions = {
            mediumScreenWidth: 6,
            smallScreenWidth: 12,
            largeScreenWidth: 3
        };
    }

    public render() : HTMLElement {
        const componentTitle = this.displayName || this.name;

        ComponentValidator.verifyComponentNamesAreSet(Component.Light, this.name, this.displayName);

        const switchContainer = RenderHelper.renderSwitch('Off', 'On', this.value as boolean);
        switchContainer.classList.add('light-component-switch');
        const componentContainer = RenderHelper.renderComponentContainer(componentTitle, switchContainer, this.columnContainerOptions);
        
        this.changeCardBackground(jquery(componentContainer).find('.card'), this.value as boolean);
        
        jquery(switchContainer).find('#switchInput').on('click', ev => {
            const newValue = jquery(ev.target).prop('checked');

            if (newValue !== this.value) {
                this.dataProvider.update(this.name, {
                    value: newValue
                })
                .then(() => {
                    this.value = newValue;
                    this.changeCardBackground(jquery(componentContainer).find('.card'), newValue);
                });
            }
        });

        return componentContainer;
    }

    private changeCardBackground(cardContainer: JQuery<HTMLElement>, componentState: boolean) : void {
        if (componentState) {
            cardContainer.addClass('yellow lighten-4');
        } else {
            cardContainer.removeClass('yellow lighten-4');
        }
    }
}
