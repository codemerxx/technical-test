import jquery from 'jquery';
import { injectable, inject, named } from 'inversify';

import { Contract, Component, Service } from '../common/enums';
import { RenderHelper, ComponentValidator } from '../utils';
import { IAutomationComponent, IDataProvider } from '../common/interfaces';
import ComponentBase from './ComponentBase';
import { ColumnOptions } from '../common/types';

@injectable()
export default class TemperatureComponent extends ComponentBase implements IAutomationComponent {
    public type: string;
    private columnContainerOptions: ColumnOptions;
    private currentTemperature: number;

    constructor(@inject(Contract.DataProvider) @named(Service.ComponentService) private dataProvider: IDataProvider) { 
        super();
        this.type = Component.Temperature;
        this.columnContainerOptions = {
            mediumScreenWidth: 6,
            smallScreenWidth: 12,
            largeScreenWidth: 4
        };
    }

    public render() : HTMLElement {
        const componentTitle = this.displayName || this.name;

        ComponentValidator.verifyComponentNamesAreSet(Component.Temperature, this.name, this.displayName);

        if (typeof this.currentTemperature === 'undefined') {
            this.currentTemperature = this.value as number;
        }

        const temperatureReadingContanier = this.renderTemperatureData('Temperature Reading', this.value);
        const setTemperatureContainer = this.renderTemperatureData('Set Temperature', this.currentTemperature);
                
        const sliderMovedCallback = (event: JQuery.TriggeredEvent) => {
            const newValue = jquery(event.target).val() as string;

            setTemperatureContainer.find('#temperatureValue')
                .html(`${newValue}&deg;C`);
        };

        const inputChangedCallback = (event: JQuery.TriggeredEvent) => {
            const newValue = jquery(event.target).val() as number;

            this.dataProvider.update(this.name, {
                value: newValue
            })
            .then(() => {
                this.currentTemperature = newValue;
                sliderMovedCallback(event);
            });
        };

        const sliderElement = jquery('<p>')
            .addClass('range-field')
            .append(jquery('<input>')
                .attr('type', 'range')
                .attr('min', 0)
                .attr('max', 40)
                .attr('value', this.currentTemperature as number)
                .change(inputChangedCallback)
                .on('input', sliderMovedCallback)
            );
        const temperatureSliderContainer = jquery('<div>')
            .addClass('row valign-wrapper')
            .append(jquery('<div>')
                .addClass('col s12 m12')
                .append(sliderElement)
            );
        
        const componentContainer = RenderHelper.renderComponentContainer(
            componentTitle,
            [ temperatureReadingContanier.get(0), setTemperatureContainer.get(0), temperatureSliderContainer.get(0) ],
            this.columnContainerOptions);

        return componentContainer;
    }

    private renderTemperatureData(dataTitle: string, dataValue: (boolean | number)) : JQuery<HTMLElement> {
        const titleContainer = jquery('<div>')
            .addClass('col s9 m9')
            .append(jquery('<h6>')
                .addClass('blue-grey-text lighten-4')
                .text(dataTitle)
            );

        const valueContainer = jquery('<div>')
            .addClass('col s3 m3')
            .append(jquery('<h5>')
                .attr('id', 'temperatureValue')
                .html(`${dataValue}&deg;C`));

        const temperatureDataContainer = jquery('<div>')
            .addClass('row valign-wrapper')
            .append(titleContainer, valueContainer);

        return temperatureDataContainer;
    }
}
