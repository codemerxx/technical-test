import LightComponent from './LightComponent';
import CurtainsComponent from './CurtainsComponent';
import TemperatureComponent from './TemperatureComponent';

export {
    LightComponent,
    CurtainsComponent,
    TemperatureComponent
};
