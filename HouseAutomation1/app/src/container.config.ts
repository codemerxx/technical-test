import 'reflect-metadata';
import { Container } from 'inversify';

import { Contract, Component, Service } from './common/enums';
import { IAutomationComponent, IDataProvider, IRequestService } from './common/interfaces';
import { LightComponent, CurtainsComponent, TemperatureComponent } from './components';
import { DefaultRequestService, ComponentDataProvider, ComponentGroupTypesProvider, LocalRequestService } from './services';
import ControlPanel from './ControlPanel';

const diContainer = new Container();

diContainer.bind<IAutomationComponent>(Contract.AutomationComponent).to(LightComponent).whenTargetNamed(Component.Light);
diContainer.bind<IAutomationComponent>(Contract.AutomationComponent).to(CurtainsComponent).whenTargetNamed(Component.Curtains);
diContainer.bind<IAutomationComponent>(Contract.AutomationComponent).to(TemperatureComponent).whenTargetNamed(Component.Temperature);

if (SERVER_TYPE === 'node') {
    diContainer.bind<IRequestService>(Contract.RequestService).to(DefaultRequestService);
} else {
    diContainer.bind<IRequestService>(Contract.RequestService).to(LocalRequestService);
}

diContainer.bind<IDataProvider>(Contract.DataProvider).to(ComponentDataProvider).whenTargetNamed(Service.ComponentService);;
diContainer.bind<IDataProvider>(Contract.DataProvider).to(ComponentGroupTypesProvider).whenTargetNamed(Service.GroupService);;
diContainer.bind(ControlPanel).toSelf().inSingletonScope();

export default diContainer;
