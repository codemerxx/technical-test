import container from './container.config';
import ControlPanel from './ControlPanel';

const controlPanel = container.get(ControlPanel);
controlPanel.initialize();
