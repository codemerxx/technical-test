import { inject, injectable } from 'inversify';

import { IDataProvider, IRequestService } from '../common/interfaces';
import { Contract, Path } from '../common/enums';
import { Response, RequestOptions } from '../common/types';

@injectable()
export default class ComponentDataProvider implements IDataProvider {

    constructor(@inject(Contract.RequestService) private requestService: IRequestService) { }

    public getAll() : Promise<any> {
        return this.requestService.get(Path.ComponentsUrl)
            .then((response: Response) => {
                return JSON.parse(response.body);
            });
    }

    public get(componentName: string) : Promise<any> {
        const componentDataUrl = [ Path.ComponentsUrl, componentName ].join('/');

        return this.requestService.get(componentDataUrl)
            .then(response => {
                return JSON.parse(response.body);
            });
    }

    public update(componentName: string, payload: any) : Promise<void> {
        const componentDataUrl = [ Path.ComponentsUrl, componentName ].join('/');
        const requestOptions: RequestOptions = {
            json: true,
            body: payload
        };

        return this.requestService.post(componentDataUrl, requestOptions)
            .then(() => {});
    }
}
