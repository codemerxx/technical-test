import { inject, injectable } from 'inversify';

import { IDataProvider, IRequestService } from '../common/interfaces';
import { Contract, Path } from '../common/enums';
import { Response, RequestOptions } from '../common/types';

@injectable()
export default class ComponentGroupTypesProvider implements IDataProvider {
    constructor(@inject(Contract.RequestService) private requestService: IRequestService) { }
    
    public getAll() : Promise<any> {
        return this.requestService.get(Path.GroupTypesUrl)
            .then((response: Response) => {
                return JSON.parse(response.body);
            });
    }

    public get(groupTypeName: string) : Promise<any> {
        const componentDataUrl = [ Path.GroupTypesUrl, groupTypeName ].join('/');

        return this.requestService.get(componentDataUrl)
            .then(response => {
                return JSON.parse(response.body);
            });
    }

    public update(groupTypeName: string, payload: any) : Promise<void> {
        const componentDataUrl = [ Path.GroupTypesUrl, groupTypeName ].join('/');
        const requestOptions: RequestOptions = {
            json: true,
            body: payload
        };

        return this.requestService.post(componentDataUrl, requestOptions)
            .then(() => {});
    }
}
