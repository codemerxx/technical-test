import { injectable } from 'inversify';

import { RequestOptions, Response as ModifiedResponse } from '../common/types';
import { IRequestService } from '../common/interfaces';

@injectable()
export default class DefaultRequestService implements IRequestService {
    public get(url: string, options?: RequestOptions) : Promise<ModifiedResponse> {
        options = {
            method: 'GET',
            ...options
        };

        return this.makeRequest(url, options);
    }

    public post(url: string, options: RequestOptions) : Promise<ModifiedResponse> {
        options = {
            method: 'POST',
            ...options
        };

        return this.makeRequest(url, options);
    }

    private makeRequest(url: string, options?: RequestOptions) : Promise<ModifiedResponse> {
        if (options.json && options.body) {
            options.body = JSON.stringify(options.body);
            options.headers = { 'Content-Type': 'application/json' };
        }

        return fetch(url, options)
            .then((response: Response) => {
                return response.text()
                    .then(responseBody => {
                        return Promise.resolve({
                            status: response.status,
                            body: responseBody
                        });
                    });
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }
}
