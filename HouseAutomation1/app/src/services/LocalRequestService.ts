import { injectable } from 'inversify';

import { RequestOptions, Response as ModifiedResponse } from '../common/types';
import { Path } from '../common/enums';
import { IRequestService } from '../common/interfaces';

const DATA_SOURCE_URL = '/dataSource/data-source.json';

@injectable()
export default class LocalRequestService implements IRequestService {
    public get(url: string, options?: RequestOptions) : Promise<ModifiedResponse> {
        return this.makeLocalRequest(url);
    }

    public post(url: string, options: RequestOptions) : Promise<ModifiedResponse> {
        return Promise.resolve({
            status: 200
        });
    }

    private makeLocalRequest(url: string) : Promise<ModifiedResponse> {
        return fetch(DATA_SOURCE_URL, { method: 'GET' })
            .then((response: Response) => {
                return response.text()
                    .then(responseBody => {
                        const modifiedBody = this.resolveDataFromUrl(url, JSON.parse(responseBody));

                        return Promise.resolve({
                            status: response.status,
                            body: JSON.stringify(modifiedBody)
                        });
                    });
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    private resolveDataFromUrl(url: string, dataSource: any) : any {
        let result = dataSource;

        if (url === Path.ComponentsUrl) {
            result = dataSource.components;
        } else if (url === Path.GroupTypesUrl) {
            result = dataSource.groupTypes;
        }

        return result;
    }
}
