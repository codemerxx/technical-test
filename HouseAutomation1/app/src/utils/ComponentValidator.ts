function verifyComponentNamesAreUnique(componentNames: string[]) : void {
    const uniqueNames = new Set<string>();

    componentNames.forEach(name => {
        if (!uniqueNames.has(name)) {
            uniqueNames.add(name);
        } else {
            throw new Error(`There is more than one component with the name ${name}.`);
        }
    });
}

function verifyComponentNamesAreSet(componentType: string, name: string, displayName: string) : void {
    if (!name || !displayName) {
        throw new Error(`${componentType} component's fields are either not initialised or invalid.`);
    }
}

export {
    verifyComponentNamesAreUnique,
    verifyComponentNamesAreSet
};
