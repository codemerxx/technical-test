import jquery from 'jquery';

import { ElementAttribute } from '../common/types';
import { ColumnOptions, ButtonOptions } from '../common/types';

function renderComponentContainer(componentName: string, componentContent: (HTMLElement | HTMLElement[]), options: ColumnOptions) : HTMLElement {
    const content = Array.isArray(componentContent) ? componentContent.map(el => jquery(el)) : jquery(componentContent);

    const cardTitleElement = jquery('<span>')
        .addClass('card-title')
        .text(componentName);

    const cardContentContainerElement = jquery('<div>')
        .addClass('card-content')
        .append(cardTitleElement, content);

    const cardContainer = jquery('<div>')
        .addClass('card')
        .append(cardContentContainerElement);

    const columnContainer = renderColumn(cardContainer.get(0), options);

    return columnContainer;
}

function renderRow(rowContent: (HTMLElement | HTMLElement[])) : HTMLElement {
    const content = Array.isArray(rowContent) ?
        rowContent.map((el: HTMLElement) => jquery(el)) :
        jquery(rowContent);

    const rowContainer = jquery('<div>')
        .addClass('row')
        .append(content);

    return rowContainer.get(0);
}

function renderColumn(columnContent: HTMLElement, options: ColumnOptions) : HTMLElement {
    const columnContainer = jquery('<div>')
        .addClass(`col s${options.smallScreenWidth} m${options.mediumScreenWidth}`)
        .append(jquery(columnContent));

    registerAttributes(columnContainer, options.attributes);

    return columnContainer.get(0);
}

function renderButton(options: ButtonOptions) : HTMLElement {
    const buttonElement = jquery('<div>')
        .addClass('btn blue-grey lighten-3')
        .text(options.text);

    registerAttributes(buttonElement, options.attributes);

    if (options.events) {
        for (const event of options.events) {
            buttonElement.on(event.type, event.callback.bind(null, buttonElement.get(0)));
        }
    }

    return buttonElement.get(0);
}

function renderSwitch(disabledText: string, enabledText: string, currentState: boolean) : HTMLElement {
    const elementInput = jquery('<input>')
        .attr('id', 'switchInput')
        .attr('type', 'checkbox')
        .prop('checked', currentState);

    const spanElement = jquery('<span>')
        .addClass('lever');

    const elementLabel = jquery('<label>')
        .append(disabledText, elementInput, spanElement, enabledText);

    const elementContainer = jquery('<div>')
        .addClass('switch')
        .append(elementLabel);

    return elementContainer.get(0);
}

function setActiveButtonGroupItem(groupElement: HTMLElement, targetButton: HTMLElement) : void {
    jquery(groupElement).children().removeClass('disabled');
    jquery(targetButton).addClass('disabled');
}

function registerAttributes(targetElement: JQuery<HTMLElement>, attributes: ElementAttribute[]) : void {
    if (attributes) {
        for (const attribute of attributes) {
            targetElement.attr(attribute.key, attribute.value);
        }
    }
}

function renderTabs(tabNames: Array<string>, selectedTabIndex: number, selectedTabCallback: (tabIndex: number) => void) : HTMLElement {
    const tabElements: Array<JQuery<HTMLElement>> = [];

    tabNames.forEach((tabName: string, tabIndex: number) => {
        const normalizedTabName = normalizeString(tabName);

        const tabItemContent = jquery('<a>')
            .attr('href', `#${normalizedTabName}`)
            .text(tabName)

        if (selectedTabIndex === tabIndex) {
            tabItemContent.addClass('active');
        }

        const listItem = jquery('<li>')
            .addClass('tab')
            .append(tabItemContent)
            .click(e => selectedTabCallback(tabIndex));

        tabElements.push(listItem);
    });

    const tabsList = jquery('<ul>')
        .addClass('tabs tabs-fixed-width')
        .append(tabElements);

    return tabsList.get(0);
}

function normalizeString(value: string) : string {
    const result = value.toLowerCase().split(' ').join('-');
    return result;
}

export {
    renderComponentContainer,
    renderRow,
    renderColumn,
    renderTabs,
    renderButton,
    normalizeString,
    setActiveButtonGroupItem,
    renderSwitch
}
