const express = require('express');
const open = require('open');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');

const app = express();
const port = process.env.port || 6969;
const clientAppRoot = path.resolve(__dirname, '../', 'app', 'dist');
const dataSourceFilePath = path.resolve(__dirname, '../', 'dataSource', 'data-source.json');

app.use(bodyParser.json());
app.use('/', express.static(clientAppRoot));

app.get('/components', (req, res) => {
    const dataSourceFileContents = getDataSourceFile();
    res.send(JSON.stringify(dataSourceFileContents.components));
});

app.get('/components/:componentName', (req, res) => {
    const components = getDataSourceFile().components;
    const componentName = req.params['componentName'];

    res.send(JSON.stringify(components[componentName]));
});

app.post('/components/:componentName', (req, res) => {
    const dataSource = getDataSourceFile();
    const newValue = typeof req.body.value === 'boolean' ? req.body.value : +req.body.value;
    const componentName = req.params['componentName'];

    const componentToEdit = dataSource.components.find(c => c.name === componentName);
    componentToEdit.value = newValue;

    updateDataSourceFile(dataSource);

    res.send();
});

app.get('/groupTypes', (req, res) => {
    const groupTypes = getDataSourceFile().groupTypes;

    res.send(JSON.stringify(groupTypes));
});

function getDataSourceFile() {
    const fileContents = fs.readFileSync(dataSourceFilePath);

    return JSON.parse(fileContents);
}

function updateDataSourceFile(newFileContents) {
    fs.writeFileSync(dataSourceFilePath, JSON.stringify(newFileContents, null, 4));
}

app.listen(port, () => {
    console.log(`App listening on port ${port}!`);
    open(`http://localhost:${port}`);
});
