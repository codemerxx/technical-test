# Credits

Webpack configuration is a derivative of the [webpack-demo-app](https://github.com/Colt/webpack-demo-app) repository.

Favicon icon is made by [geotatah](https://www.flaticon.com/authors/geotatah) from [www.flaticon.com](https://www.flaticon.com/) and generated with the [realfavicongenerator.net](https://realfavicongenerator.net/) online tool.