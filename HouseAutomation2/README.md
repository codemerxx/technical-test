# Adobe House Automation :bulb:
Adobe house automation is web application for house automation. It is easily extensible and allows adding custom components for controlling different aspects of the house automation - from turning on a light, to setting the thermostat.

## Demo
There is a packaged version of the application in the `packaged` directory, ready to be served by an generic HTTP server.

## Supported Browsers

The application has been tested extensively on Chrome and Mozilla.

## Build instructions

### Prerequisites
- Node.js
- NPM

### Build procedure
1. npm install
2. npm run build:prod

The application will be outputted to the `dist` directory, and can be served by generic HTTP server.

## Architecture
The main authority in the application is the `Mediator` class. It is Singleton, since only one instance should be created for the application life cycle. All of its dependencies are injected though interfaces in the constructor, so they are easily substitutable. The main purpose of the `Mediator` class is to render the application and its components. Registered rooms and components are configured in the `src/api/component-configuration.json` file and passed to the Mediator though its `configuration` argument. The `componentFactories` argument is a map of component name to factory method for component creation. This is used to create component instances based on the configuration. Lastly, the `dataServiceFactory` argument is a factory for creating `DataService` instances, which are used by the components to access their data.

Components are defined by `.component.ts` files in the `src/app/components` directory. All `.component.ts` files are bundled in the application compile-time by Webpack. Their `exports` are processed by the `src/app/component/index.ts` file at runtime and the component factories map is created.

Components should implement the `Component` interface. It defines a field `size` and a `render()` method. The `render()` method will be called by the application, allowing the component to render itself. Single `HTMLElement` should be returned, which will be rendered in container with size defined by the `size` field. This architecture allows fully custom `HTML`, `CSS` and `JavaScript` code to be used, ensuring high level of customizability.

## Guide: how to create new components

### Creating the actual component
In order to implement new type of component, one must:
1. Create new `.component.ts` file in the `src/app/components` folder
2. Create new class implementing the `Component` interface
3. Export object with 2 properties: `name` and `factory`. The value of the `name` property should be a string, defining the unique type identifier (a.k.a. `ComponentTypeId`) of this component. The value of the `factory` property should be a function, accepting `DataService<GetType, SetType>` instance and returning instance of the component.

Custom SASS stylesheets can be used by importing them into the `.component.ts` file like this:
```
import styles from './myComponent.component.scss';
```
Multiple stylesheet files can be imported. While the naming of the files is not restricted in any way, following the `.component.scss` naming convention is encouraged.

### Registering component instances
Component instances are defined in the `src/api/component-configuration.json` file. The file schema is as follows:
```
{
    "Room display name" : {
        "<ComponentTypeId>": [{
            "id": "Unique component identifier",
            "name": "Component display name"
        }]
    }
}
```

### Adding data for all registered components
All the data for all component instances is contained in the `src/api` folder. It contains number of subfolders equal to the number of registered `ComponentTypeId`s. Every subfolder contains number of `.json` files equal to the number of registered component instances from the given `ConponentTypeId` and their names should match the unique component identifier, defined in the `src/api/component-configuration.json` file.

The format of the data is not restricted in any way and it is completely up to the component creator to know what data is needed to the component to function correctly.

## API documentation

### ComponentSize
Enumeration describing the possible values of component sizing.

Values:
- `ComponentSize.Small`
- `ComponentSize.Medium`
- `ComponentSize.Large`

### Component
Interface allowing creation of new component types.

Fields:
- `readonly size: ComponentSize` - set by `Component` implementation to define what size container is needed in order to display the component correctly.

Methods:
- `render(): Promise<HTMLElement>` - called by the application when loading the page. The returned HTML element is added to the DOM tree.

### DataService<T, K>
Interface used to allow `Component` instances to retrieve data (of type T) and persist data (Of type K) as needed.

Methods:
- `get(): Promise<T>` - Reads the data for the `Component` from the data source
- `set(K): Promise<void>` - Writes the provided data to the data source for this `Component`
