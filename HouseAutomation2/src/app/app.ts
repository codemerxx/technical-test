import { Mediator } from './mediator';
import { JQueryHttpClient } from './data/jquery-http-client';
import { Configuration } from './configuration';
import componentFactories from './components';
import { StaticFileDataServiceFactory } from './data/static-file-data-service-factory';

export const run = async () => {
    const httpClient = new JQueryHttpClient();
    const dataServiceFactory = new StaticFileDataServiceFactory(httpClient);
    const configuration = await httpClient.get<Configuration>('api/component-configuration.json');
    const mediator = await Mediator.getInstance(configuration, componentFactories, dataServiceFactory);
    await mediator.render();
};