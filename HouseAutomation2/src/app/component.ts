import { DataService } from './data/data-service';

export interface Component {
    readonly size: ComponentSize;

    render(): Promise<HTMLElement>;
}

export enum ComponentSize {
    Small,
    Medium,
    Large
}

export type ComponentFactory = (dataService: DataService<any, any>) => Component;
