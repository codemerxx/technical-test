import styles from './curtains.component.scss';

import { Component, ComponentSize } from '../component';
import { DataService } from '../data/data-service';

class CurtainsComponent implements Component {
    public readonly size: ComponentSize = ComponentSize.Small;

    private value: boolean = false;

    constructor(private dataService: DataService<boolean, boolean>) { }

    public async render(): Promise<HTMLElement> {
        this.value = await this.dataService.get();
        const container = $('<div></div>');
        if (this.value) {
            container.addClass(styles.curtainsOn);
        }
        const button = $('<button></button>').html(this.value ? 'Open' : 'Close')
                                             .on('click', () => {
            this.value = !this.value;
            button.html(this.value ? 'Open' : 'Close');
            this.dataService.set(this.value);
            container.toggleClass(styles.curtainsOn);
        });
        container.append(button);
        return container.get(0);
    }
}

export const name = 'curtains';

export const factory = (dataService: DataService<boolean, boolean>) => new CurtainsComponent(dataService);
