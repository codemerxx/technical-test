import { ComponentFactory } from "../component";

const componentsContext = require.context('./', false, /\.component\.ts/);

const componentFactories = new Map<string, ComponentFactory>();
componentsContext.keys().forEach(key => {
    const component = componentsContext(key);
    componentFactories.set(component.name, component.factory);
});

export default componentFactories;
