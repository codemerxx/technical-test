import styles from './light.component.scss';

import { Component, ComponentSize } from '../component';
import { DataService } from '../data/data-service';

class LightComponent implements Component {
    public readonly size: ComponentSize = ComponentSize.Medium;
    private value: boolean = false;

    constructor(private dataService: DataService<boolean, boolean>) { }

    public async render(): Promise<HTMLElement> {
        this.value = await this.dataService.get();
        const container = $('<div></div>');
        if (this.value) {
            container.addClass(styles.lightsOn);
        }
        const button = $('<button></button>').html(this.value ? 'Turn off' : 'Turn on')
                                             .on('click', () => {
            this.value = !this.value;
            button.html(this.value ? 'Turn off' : 'Turn on');
            this.dataService.set(this.value);
            container.toggleClass(styles.lightsOn);
        });
        container.append(button);
        return container.get(0);
    }
}

export const name = 'light';

export const factory = (dataService: DataService<boolean, boolean>) => new LightComponent(dataService);
