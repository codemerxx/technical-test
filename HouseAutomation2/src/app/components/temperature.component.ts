import styles from './temperature.component.scss';

import { Component, ComponentSize } from "../component";
import { DataService } from "../data/data-service";

type Data = {
    set: number,
    actual: number
};

class TemperatureComponent implements Component {
    public readonly size: ComponentSize = ComponentSize.Large;

    private data: Data = { set: 0, actual: 0 };

    constructor(private dataService: DataService<Data, number>) { }

    public async render(): Promise<HTMLElement> {
        this.data = await this.dataService.get();
        const container = $('<div />');

        const setTemperatureValue = $('<span />').html(this.data.set.toString());
        const setTemperatureLabel = $('<div />').append('Set: ')
                                                .append(setTemperatureValue);
        const currentTemperatureLabel = $('<div />').append(`Current: ${this.data.actual}`);
        const slider = $('<input />').attr('type', 'range')
                                     .attr('min', 15)
                                     .attr('max', 30)
                                     .val(this.data.set);
        slider.on('input', () => {
            this.data.set = Number(slider.val());
            setTemperatureValue.html(this.data.set.toString());
            this.dataService.set(this.data.set);
        });
        container.append(setTemperatureLabel, currentTemperatureLabel, slider);
        return container.get(0);
    }
}

export const name = 'temperature';

export const factory = (dataService: DataService<Data, number>) => new TemperatureComponent(dataService);
