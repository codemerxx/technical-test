export type Configuration = {
    [key: string]: {
        [key: string]: [{
            id: string,
            name: string
        }]
    }
};
