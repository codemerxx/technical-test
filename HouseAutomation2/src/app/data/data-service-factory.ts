import { DataService } from './data-service';

export interface DataServiceFactory {
    create(url: string): DataService<any, any>;
}
