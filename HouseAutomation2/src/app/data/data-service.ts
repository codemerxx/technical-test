export interface DataService<T, K> {
    get(): Promise<T>;
    set(data: K): Promise<void>;
}
