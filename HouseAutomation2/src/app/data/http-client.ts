export interface HttpClient {
    get<T>(url: string): Promise<T>;
    
    post<T>(url: string, data?: object | string): Promise<T>;
}
