import { HttpClient } from './http-client';

export class JQueryHttpClient implements HttpClient {
    public get<T>(url: string): Promise<T> {
        return this.send<T>('GET', url);
    }
    
    public post<T>(url: string, data?: object | string): Promise<T> {
        return this.send<T>('POST', url, data);
    }

    private send<T>(method: string, url: string, data?: object | string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            $.ajax({
                url: url,
                method: method,
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json',
                success: res => resolve(res),
                error: err => reject(err)
            });
        });
    }
}
