import { DataService } from './data-service';
import { DataServiceFactory } from './data-service-factory';
import { StaticFileDataService } from './static-file-data-service';
import { HttpClient } from './http-client';

export class StaticFileDataServiceFactory implements DataServiceFactory {
    public constructor(private httpClient: HttpClient) { }

    public create(url: string): DataService<any, any> {
        return new StaticFileDataService(this.httpClient, url);
    }
}
