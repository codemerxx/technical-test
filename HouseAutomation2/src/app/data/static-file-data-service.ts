import { DataService } from './data-service';
import { HttpClient } from './http-client';

export class StaticFileDataService implements DataService<any, any> {
    constructor(private httpClient: HttpClient, private url: string) { }

    public get(): Promise<any> {
        return this.httpClient.get(this.url + '.json');
    }
    
    public set(data: any): Promise<void> {
        return Promise.resolve();
    }
}
