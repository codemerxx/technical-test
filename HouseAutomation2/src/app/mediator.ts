import { ComponentFactory, Component, ComponentSize } from './component'
import { Configuration } from './configuration';
import { DataServiceFactory } from './data/data-service-factory';

export class Mediator {
    private static instance: Mediator;

    private constructor(
        private configuration: Configuration,
        private componentFactories: Map<string, ComponentFactory>,
        private dataServiceFactory: DataServiceFactory
    ) { }

    static getInstance(
        configuration: Configuration,
        componentFactories: Map<string, ComponentFactory>,
        dataServiceFactory: DataServiceFactory
    ): Mediator {
        if (!Mediator.instance) {
            Mediator.instance = new Mediator(configuration, componentFactories, dataServiceFactory);
        }

        return Mediator.instance;
    }

    public async render(): Promise<void> {
        for (const [roomName, roomConfiguration] of Object.entries(this.configuration)) {
            const components = new Array<JQuery<HTMLElement>>();
            for (const [componentType, componentInstances] of Object.entries(roomConfiguration)) {
                const componentFactory = this.componentFactories.get(componentType);
                if (!componentFactory) {
                    continue;
                }

                for (const { id, name } of componentInstances) {
                    let component: Component;
                    let componentContent: HTMLElement;
                    try {
                        component = componentFactory(this.dataServiceFactory.create(`/api/${componentType}/${id}`));
                        componentContent = await component.render();
                    } catch(e) {
                        console.log(e);
                        continue;
                    }
                    components.push(Mediator.createComponent(name, $(componentContent), component.size));
                }
            }
            $('.rooms-container').append(Mediator.createRoom(roomName, components));
        }
    }

    private static createContainer(): JQuery<HTMLElement> {
        return Mediator.createElement('div');
    }

    private static createRoom(name: string, components: Array<JQuery<HTMLElement>>): JQuery<HTMLElement> {
        const titleContainer = Mediator.createElement('h5')
                                       .text(name);
        return Mediator.createContainer()
                       .addClass('col-lg-12 col-xl-6 mt-4 mb-4')
                       .append(Mediator.createCard(titleContainer, Mediator.createContainer()
                                                                           .addClass('row')
                                                                           .append(components)));
    }

    private static createComponent(name: string, content: JQuery<HTMLElement>, size: ComponentSize): JQuery<HTMLElement> {
        const titleContainer = Mediator.createContainer()
                                       .text(name);
        return Mediator.createContainer()
                       .addClass(`${Mediator.getComponentContainerSize(size)} mb-2 mt-2`)
                       .append(Mediator.createCard(titleContainer, content));
    }

    private static createCard(cardTitle: JQuery<HTMLElement>, content: JQuery<HTMLElement>): JQuery<HTMLElement> {
        return Mediator.createContainer()
                       .addClass('card')
                       .append(cardTitle.addClass('card-header'))
                       .append(content.addClass('card-body'));
    } 

    private static createElement(element: string): JQuery<HTMLElement> {
        return $(`<${element}></${element}>`);
    }

    private static getComponentContainerSize(componentSize: ComponentSize) : string {
        switch (componentSize) {
            case ComponentSize.Small:
                return 'col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3';
            case ComponentSize.Medium:
                    return 'col-12 col-sm-7 col-md-6 col-lg-5 col-xl-4';
            case ComponentSize.Large:
                return 'col-12 col-sm-8 col-md-7 col-lg-6 col-cl-5';
            default:
                throw new Error('Invalid ComponentSize');
        };
    }
}