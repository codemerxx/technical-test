import './assets/favicons/favicons';
import './main';
import 'popper.js';
import 'bootstrap';
(window as any).bsCustomFileInput = require('bs-custom-file-input');
import 'mdbootstrap';
import { run } from './app/app';

run();