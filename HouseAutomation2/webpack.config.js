const isDevelopment = process.env.NODE_ENV !== 'production';

const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  devtool: isDevelopment ? 'inline-source-map' : false,
  entry: './src/index.ts',
  output: {
    filename: isDevelopment ? '[name].bundle.js' : '[name].[contentHash].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: isDevelopment ? undefined : {
    minimizer: [
      new OptimizeCssAssetsPlugin(),
      new TerserPlugin(),
      new HtmlWebpackPlugin({
        template: './src/index.html',
        minify: {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          removeComments: true
        }
      })
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.scss', '.sass']
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new CopyPlugin([
      { from: 'src/api/', to: 'api/' }
    ]),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: isDevelopment ? '[name].css' : '[name].[contentHash].css',
      chunkFilename: isDevelopment ? '[id].css' : '[id].[contentHash].css'
    }),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ['html-loader']
      },
      {
        test: /\.s(a|c)ss$/,
        oneOf: [
          {
            test: /\.component\.s(a|c)ss$/,
            use: [
              isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
              {
                loader: "typings-for-scss-modules-loader",
                options: {
                  modules: true,
                  namedExport: true,
                  camelCase: true,
                  onlyLocals: false,
                  sourceMap: isDevelopment
                }
              },
              {
                loader: "sass-loader",
                options: {
                  sourceMap: isDevelopment
                }
              }
            ]
          },
          {
            use: [
              isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
              {
                loader: "css-loader",
                options: {
                  sourceMap: isDevelopment
                }
              },
              {
                loader: "sass-loader",
                options: {
                  sourceMap: isDevelopment
                }
              }
            ]
          }
        ]
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  }
};